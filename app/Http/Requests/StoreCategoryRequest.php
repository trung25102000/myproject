<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;
use Illuminate\Validation\ValidationException;

class StoreCategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'CategoryName' => 'required|max:50',
            'Description' => 'required',
            'ParentCategoryID' => 'required|numeric'
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        $response=new Response([
            'errors'=>$validator->errors()
            ],Response::HTTP_UNPROCESSABLE_ENTITY
        );

        throw (new ValidationException($validator,$response));
    }

}
