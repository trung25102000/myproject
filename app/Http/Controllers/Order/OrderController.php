<?php

namespace App\Http\Controllers\Order;

use App\Http\Controllers\Controller;
use App\Models\Cart;
use App\Models\Customer;
use App\Models\Employees;
use App\Models\Order;
use App\Models\OrderDetails;
use App\Models\Products;
use App\Models\Shipper;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    protected $Order;

    /**
     * @param $Order
     */
    public function __construct(Order $Order)
    {
        $this->Order = $Order;
    }


    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $OrderList = $this->Order->paginate(10);
        return view('admin.Order.Index', ['Order' => $OrderList]);
    }

    public function search(Request $request)
    {
        $perPage = 10;

        $searchValue = $request->input('searchValue'); // từ khóa tìm kiếm
        $Status = $request->input('Status');
        $request->session()->put('searchValue', $searchValue);
        $OrderList = $this->Order::select('orders.*', 'customers.CustomerName', 'customers.ContactName as ContactName', 'customers.Address as CustomerAddress', 'customers.Email as CustomerEmail', 'employees.FullName as EmployeeFullName', 'shippers.ShipperName', 'shippers.Phone as ShipperPhone')
            ->leftJoin('customers', 'orders.customer_id', '=', 'customers.ID')
            ->leftJoin('employees', 'orders.employees_id', '=', 'employees.ID')
            ->leftJoin('shippers', 'orders.Shipper_id', '=', 'shippers.ID')
            ->where(function ($query) use ($Status, $searchValue) {
                if ($Status != '0') {
                    $query->where('orders.Status', 'like', $Status);
                }
                if (!empty($searchValue)) {
                    $query->where(function ($query) use ($searchValue) {
                        $query->where('customers.CustomerName', 'like', '%' . $searchValue . '%')
                            ->orWhere('shippers.ShipperName', 'like', '%' . $searchValue . '%');
                    });
                }
            })
            ->paginate(10);
        return view('admin.Order.Search', ['Order' => $OrderList]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $Product = Products::paginate(10);
        $Customer = Customer::all();
        $Employee = Employees::all();
        return view('admin.Order.Create', compact('Customer', 'Employee', 'Product'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'OrderName' => 'required|max:50',
            'Phone' => 'required'
        ], [
            'OrderName.required' => 'Tên người giao hàng không được bỏ trống',
            'OrderName.max' => 'Tên người giao hàng không được vượt quá 50 kí tự',
            'Phone.required' => 'Số điện thoại không được bỏ trống'
        ]);

        // Xử lý lưu dữ liệu
        $dataRequest = $request->all();

        $Order = $this->Order->create($dataRequest);


        return redirect()->route('Order.index')->with('success', 'Thêm mới người giao hàng: ' . $Order->OrderName . ' thành công');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $Order = $this->Order->findorfail($id);

        return view('admin.Order.Delete', compact('Order'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $Order = $this->Order->findorfail($id);
        return view('admin.Order.Edit', compact('Order'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $request->validate([
            'OrderName' => 'required|max:50',
            'Phone' => 'required'
        ], [
            'OrderName.required' => 'Tên người giao hàng không được bỏ trống',
            'OrderName.max' => 'Tên người giao hàng không được vượt quá 50 kí tự',
            'Phone.required' => 'Số điện thoại không được bỏ trống'
        ]);

        $dataUpdate = $request->all();

        $Order = $this->Order->findOrFail($id);

        $Order->update($dataUpdate);

        return redirect()->route('Order.index')->with(['message' => 'Update  Order: ' . $Order->OrderName . " success"]);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $Order = $this->Order->findorfail($id);

        $Order->delete($Order);

        return redirect()->route('Order.index')->with(['message' => 'Delete  Order: ' . $Order->OrderName . ' success']);
    }

    public function searchproduct(Request $request)
    {
        $searchValue = $request->input('searchValue'); // từ khóa tìm kiếm
        $request->session()->put('searchValue', $searchValue);
        $ProductList = Products::where(function ($query) use ($searchValue) {
            $query->where('ProductName', 'like', "%$searchValue%")
                ->orWhere('Price', 'like', "%$searchValue%")
                ->orWhere('Unit', 'like', "%$searchValue%");
        })->paginate(10);

        return view('admin.Order.SearchProduct', ['Product' => $ProductList]);
    }

    public function AddToCart(Request $request)
    {
        $productId = $request->input('products_id');
        $ProductName = $request->input('ProductName');
        $Unit = $request->input('unit');
        $Photo = $request->input('Photo');
        $SalePrice = $request->input('Price');
        $Quantity = $request->input('Quantity');
        $product = Products::find($productId);

        if (!$product) {
            return redirect()->back()->with('error', 'Sản phẩm không tồn tại.');
        }

        if (!$Quantity || !is_numeric($Quantity) || $Quantity <= 0) {
            return redirect()->back()->with('error', 'Số lượng sản phẩm không hợp lệ.');
        }
        // Thêm sản phẩm vào giỏ hàng (có thể lưu vào session hoặc cơ sở dữ liệu)
        // Ví dụ lưu vào session:
        $cart = session()->get('cart', []);

        if (isset($cart[$productId])) {
            $cart[$productId]['Quantity'] += $Quantity;
        } else {
            $cart[$productId] = [
                'Product_id' => $product->id,
                'ProductName' => $product->ProductName,
                'Unit' => $product->Unit,
                'Price' => $product->Price,
                'Quantity' => $Quantity,
                'ToTalPrice' => $product->Price * $product->Quantity
            ];
        }

        session()->put('cart', $cart);

        return redirect()->back()->with('success', 'Sản phẩm đã được thêm vào giỏ hàng.');
    }

    /**
     * @param $productId
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function removeFromCart($productId)
    {
        $cart = session()->get('cart', []);

        if ($cart != null) {
            if (isset($cart[$productId])) {
                unset($cart[$productId]);
                session()->put('cart', $cart);
                if (count($cart) == 0) session()->forget('cart');
                return redirect()->back()->with('success', 'Sản phẩm đã được xóa khỏi giỏ hàng.');
            }
            return redirect()->back()->with('error', 'Sản phẩm không tồn tại trong giỏ hàng.');
        } else return redirect()->back()->with('error', 'Giỏ hàng không tồn tại!');
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function RemoveCart()
    {
        $cart = session()->get('cart', []);
        if ($cart != null) {
            session()->forget('cart');
            return redirect()->back()->with('success', 'Giỏ hàng đã xóa.');
        } else  return redirect()->back()->with('error', 'Giỏ hàng trống');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Foundation\Application
     */
    public function Init(Request $request)
    {
        $cart = session()->get('cart', []);
        $customer_ID = $request->input('customerID');
        $employees_ID = $request->input('employeeID');

        if ($cart == null) {
            return redirect()->back()->with('error', 'Không thể lập đơn hàng với giỏ hàng trống!');
        }
        if ($customer_ID == 0) {
            return redirect()->back()->with('error', 'Vui lòng chọn khách hàng!');
        }
        if ($employees_ID == 0) {
            return redirect()->back()->with('error', 'Vui lòng chọn nhân viên!');
        }
        $Customer = Customer::findOrFail($customer_ID);
        $Employee = Employees::findOrFail($employees_ID);

        $ShipperList=Shipper::all();

        $order = new Order();
        $order->customer_id = $customer_ID;
        $order->employees_id = $employees_ID;
        $order->OrderTime = now(); // Thời gian đặt hàng
        $order->Status = 'Đơn hàng mới (chờ duyệt)!'; // Trạng thái mặc định
        $order->save();
        // Lưu chi tiết đơn hàng từ giỏ hàng
        if (session()->has('cart')) {
            $cartItems = session('cart');
            foreach ($cartItems as $item) {
                $orderDetail = new OrderDetails();
                $orderDetail->order_id = $order->id;
                $orderDetail->products_id = $item['Product_id'];
                $orderDetail->Quantity = $item['Quantity'];
                $orderDetail->SalePrice = $item['Price'];
                $orderDetail->save();
            }
            // Xóa giỏ hàng
            session()->forget('cart');
        }
        $order_id = $order->id;
        $OrderDetailsList = OrderDetails::where('order_id', 'LIKE', '%' . $order_id . '%')->get();
        return view('admin.Order.Details', compact('Customer', 'Employee', 'order', 'OrderDetailsList','ShipperList'));
    }

    /**
     * @param $productId
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function RemoveDetail($productId, $orderid)
    {
        $ShipperList=Shipper::all();

        $OrderDetails = OrderDetails::where('order_id', 'LIKE', '%' . $orderid . '%')
            ->where('products_id', 'LIKE', '%' . $productId . '%')->delete();
        $order = $this->Order->findOrFail($orderid);
        $order_id = $order->id;
        $Customer = Customer::findOrFail($order->customer_id);
        $Employee = Employees::findOrFail($order->employees_id);
        $OrderDetailsList = OrderDetails::where('order_id', 'LIKE', '%' . $order_id . '%')->get();
        return view('admin.Order.Details', compact('Customer', 'Employee', 'order', 'OrderDetailsList','ShipperList'));

    }

    /**
     * @param $OrderId
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Foundation\Application
     */
    public function Details($OrderId)
    {
        $ShipperList=Shipper::all();
        $order = $this->Order->findOrFail($OrderId);
        $order_id = $order->id;
        $Customer = Customer::findOrFail($order->customer_id);
        $Employee = Employees::findOrFail($order->employees_id);
        $OrderDetailsList = OrderDetails::where('order_id', 'LIKE', '%' . $order_id . '%')->get();
        return view('admin.Order.Details', compact('Customer', 'Employee', 'order', 'OrderDetailsList','ShipperList'));
    }

    /**
     * @param $OrderId
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Foundation\Application
     */
    public function Accept($OrderId)
    {
        $ShipperList=Shipper::all();
        $order = $this->Order->findOrFail($OrderId);
        $order->AcceptTime=now();
        $order->Status='Đơn hàng đã duyệt (chờ chuyển hàng)!';
        $order->save();
        $order_id = $order->id;
        $Customer = Customer::findOrFail($order->customer_id);
        $Employee = Employees::findOrFail($order->employees_id);
        $OrderDetailsList = OrderDetails::where('order_id', 'LIKE', '%' . $order_id . '%')->get();
        return view('admin.Order.Details', compact('Customer', 'Employee', 'order', 'OrderDetailsList','ShipperList'));
    }

    /**
     * @param $OrderId
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Foundation\Application
     */
    public function Shipping($OrderId)
    {
        $ShipperList=Shipper::all();
        $Order=$this->Order->find($OrderId);
        return view('admin.Order.Shipping',['Shipper'=>$ShipperList,'Order'=>$Order]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Foundation\Application
     */
    public function Shipped(Request $request)
    {
        $OrderId=$request->input('OrderId');
        $ShipperId=$request->input('ShipperID');

        $Shipper=Shipper::findOrFail($ShipperId);
        if($Shipper!=null)
        {
            $order = $this->Order->findOrFail($OrderId);
            $order->shipper_id=$Shipper->id;
            $order->ShippedTime=now();
            $order->Status='Đơn hàng đang được giao';
            $order->save();
        }

        $ShipperList=Shipper::all();

        $order_id = $order->id;
        $Customer = Customer::findOrFail($order->customer_id);
        $Employee = Employees::findOrFail($order->employees_id);
        $OrderDetailsList = OrderDetails::where('order_id', 'LIKE', '%' . $order_id . '%')->get();
        return view('admin.Order.Details', compact('Customer', 'Employee', 'order', 'OrderDetailsList','ShipperList','Shipper'));
    }

    /**
     * @param $OrderId
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Foundation\Application
     */
    public function Finished($OrderId)
    {
        $ShipperList=Shipper::all();
        $order = $this->Order->findOrFail($OrderId);
        $order_id = $order->id;
        $order->FinishedTime=now();
        $order->Status='Đơn hàng đã hoàn tất thành công';
        $order->save();
        $Customer = Customer::findOrFail($order->customer_id);
        $Employee = Employees::findOrFail($order->employees_id);
        $OrderDetailsList = OrderDetails::where('order_id', 'LIKE', '%' . $order_id . '%')->get();
        return view('admin.Order.Details', compact('Customer', 'Employee', 'order', 'OrderDetailsList','ShipperList'));
    }

    /**
     * @param $OrderId
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Foundation\Application
     */
    public function Reject($OrderId)
    {
        $order = $this->Order->findOrFail($OrderId);
        $order_id = $order->id;
        $order->Status='Đơn hàng bị từ chối';
        $order->save();
        $OrderList = $this->Order->paginate(10);
        return view('admin.Order.Index', ['Order' => $OrderList]);
    }

    /**
     * @param $OrderId
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Foundation\Application
     */
    public function Cancel($OrderId)
    {
        $order = $this->Order->findOrFail($OrderId);
        $order_id = $order->id;
        $order->Status='Đơn hàng bị hủy';
        $order->save();
        $OrderList = $this->Order->paginate(10);
        return view('admin.Order.Index', ['Order' => $OrderList]);
    }
}
