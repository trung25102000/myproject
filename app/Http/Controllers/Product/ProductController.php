<?php

namespace App\Http\Controllers\Product;

use App\Http\Controllers\Controller;
use App\Models\categories;
use App\Models\ProductAttributes;
use App\Models\ProductPhotos;
use App\Models\Products;
use App\Models\Suppliers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class ProductController extends Controller
{

    protected $Product;

    protected $ProductPhoto;

    protected $ProductAttribute;



    /**
     * @param $Product
     */
    public function __construct(Products $Product)
    {
        $this->Product = $Product;
    }


    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $Product=$this->Product->latest('id')->paginate(10);

        return view('admin.Product.Index',compact('Product'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $Category=categories::all();
        $Supplier=Suppliers::all();
        return view('admin.Product.Create',compact('Category','Supplier'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'ProductName' => 'required|max:50',
            'categories_id' => 'required',
            'suppliers_id' => 'required',
            'Unit' => 'required',
            'Price' => 'required',
            'UploadPhoto' =>'required'
        ], [
            'ProductName.required' => 'Tên sản phảm không được bỏ trống',
            'ProductName.max' => 'Tên sản phẩm không được vượt quá 50 kí tự',
            'Unit.required' => 'Đơn vị tính không được bỏ trống',
            'categories_id.required' => 'Loại hàng không được bỏ trống',
            'suppliers_id.required' => 'Nhà cung cấp không được bỏ trống',
            'Price.required' => 'Giá cả không được bỏ trống',
            'UploadPhoto.required' => 'Ảnh không được bỏ trống',
            'UploadPhoto.image' => 'Ảnh không đúng định dạng',
            'UploadPhoto.mimes' => 'Ảnh không đúng định dạng',
            'UploadPhoto.max' => 'Dung lượng ảnh quá lớn'
        ]);

        // Xử lý lưu dữ liệu
        $dataRequest=$request->all();
        if ($request->hasFile('UploadPhoto')) {
            $file = $request->file('UploadPhoto');
            $fileName =  $file->getClientOriginalName();
            //$filePath =('public/Images/'.$fileName);
            $file->move('Images',$fileName);
            $dataRequest['Photo'] = 'public/Images/'.$fileName;
        }

        $Product=$this->Product->create($dataRequest);


        return redirect()->route('Product.index')->with('success', 'Thêm mới loại hàng '.$Product->ProductName.' thành công');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $Category=categories::all();
        $Supplier=Suppliers::all();
        $Product=$this->Product->findorfail($id);

        return view('admin.Product.Delete',compact('Product','Category','Supplier'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $Product=$this->Product->findorfail($id);
        $ProductPhoto=ProductPhotos::where('products_id', $id)->get();
        $ProductAttribute=ProductAttributes::where('products_id', $id)->get();
        $Category=categories::all();
        $Supplier=Suppliers::all();
        return view('admin.Product.Edit',compact('Product','ProductPhoto','ProductAttribute','Category','Supplier'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request,$id)
    {
            $request->validate([
                'ProductName' => 'required|max:50',
                'categories_id' => 'required',
                'suppliers_id' => 'required',
                'Unit' => 'required',
                'Price' => 'required',
                'Photo'=>'required'
            ], [
                'ProductName.required' => 'Tên sản phảm không được bỏ trống',
                'ProductName.max' => 'Tên sản phẩm không được vượt quá 50 kí tự',
                'Unit.required' => 'Đơn vị tính không được bỏ trống',
                'categories_id.required' => 'Loại hàng không được bỏ trống',
                'suppliers_id.required' => 'Nhà cung cấp không được bỏ trống',
                'Price.required' => 'Giá cả không được bỏ trống',
                'Photo.required' => 'Ảnh không được bỏ trống',
                'UploadPhoto.image' => 'Ảnh không đúng định dạng',
                'UploadPhoto.mimes' => 'Ảnh không đúng định dạng',
                'UploadPhoto.max' => 'Dung lượng ảnh quá lớn'
            ]);


            $dataUpdate = $request->all();

            $Product = $this->Product->findOrFail($id);

            if ($request->hasFile('UploadPhoto')) {
                $file = $request->file('UploadPhoto');
                $fileName =  $file->getClientOriginalName();
                //$filePath =('public/Images/'.$fileName);
                $file->move('Images',$fileName);
                $dataUpdate['Photo'] = 'public/Images/'.$fileName;
            }

            $Product->update($dataUpdate);



            return redirect()->route('Product.index')->with(['message' => 'Update  Product: '. $Product->ProductName." success"]);


    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $Product=$this->Product->findorfail($id);

        $Product->delete($Product);

        return redirect()->route('Product.index')->with(['message' => 'Delete  Product: '.$Product->ProductName.' success']);
    }

    public function search(Request $request)
    {
        $searchValue = $request->input('searchValue');

        $Product = $this->Product
            ->where('ProductName', 'like', "%$searchValue%")
            ->orWhere('Unit', 'like', "%$searchValue%")
            ->orWhere('Price', 'like', "%$searchValue%")
            ->latest('id')
            ->paginate(10);

        return view('admin.Product.Index', compact('Product'));
    }
}
