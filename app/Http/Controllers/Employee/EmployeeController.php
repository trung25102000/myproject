<?php

namespace App\Http\Controllers\Employee;

use App\Http\Controllers\Controller;
use App\Models\Employees;
use Illuminate\Http\Request;

class EmployeeController extends Controller
{

    protected $Employee;

    /**
     * @param $Employee
     */
    public function __construct(Employees $Employee)
    {
        $this->Employee = $Employee;
    }


    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $EmployeeList=$this->Employee->paginate(10);
        return view('admin.Employee.Index',['Employee'=>$EmployeeList]);
    }

    public function search(Request  $request)
    {
        $perPage = 10;

        $searchValue = $request->input('searchValue'); // từ khóa tìm kiếm


        $EmployeeList = $this->Employee::where(function ($query) use ($searchValue) {
            $query->where('FullName', 'like', "%$searchValue%")
                ->orWhere('Notes', 'like', "%$searchValue%");
        })->paginate(10);

        return view('admin.Employee.Search', ['Employee' => $EmployeeList]);
    }
    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
