<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreCategoryRequest;
use App\Http\Resources\CategoryResource;
use App\Models\categories;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class CategoryController extends Controller
{
    protected $category;

    /**
     * @param $category
     */
    public function __construct(categories $category)
    {
        $this->category = $category;
    }


    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $category=$this->category->paginate(10);

        $categoryResource=CategoryResource::collection($category)->response()->getData(true);


        return response()->json(['data'=>$categoryResource],Response::HTTP_OK);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreCategoryRequest $request)
    {
        //lấy dữ liệu về từ resquest
        $DataRequest=$request->all();

        $Category=$this->category->create($DataRequest);

        $categoryResource=  new CategoryResource($Category);

        return response()->json(['data'=>$categoryResource],Response::HTTP_OK);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $Category=$this->category->findorfail($id);

        $CategoryResource= new CategoryResource($Category);

        return response()->json(['data'=>$CategoryResource],Response::HTTP_OK);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //lấy dữ liệu về từ resquest
        $DataRequest=$request->all();

        $Category=$this->category->findorfail($id);

        $Category->update($DataRequest);

        $CategoryResource= new CategoryResource($Category);

        return response()->json(['data'=>$CategoryResource],Response::HTTP_OK);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $data =$this->category->findorfail($id);
        if ($data) {
            $data->delete();
            return response()->json(['message' => 'Xóa thành công'], Response::HTTP_OK);
        } else {
            return response()->json(['message' => 'Không tìm thấy dữ liệu'],Response::HTTP_NOT_FOUND);
        }
    }
}
