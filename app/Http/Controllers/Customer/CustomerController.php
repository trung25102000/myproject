<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use App\Models\Customer;
use Illuminate\Http\Request;

class CustomerController extends Controller
{
    protected $Customer;

    /**
     * @param $Customer
     */
    public function __construct(Customer $Customer)
    {
        $this->Customer = $Customer;
    }


    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $CustomerList=$this->Customer->paginate(10);
        return view('admin.Customer.Index',['Customer'=>$CustomerList]);
    }

    public function search(Request  $request)
    {
        $perPage = 10;

        $searchValue = $request->input('searchValue'); // từ khóa tìm kiếm


        $CustomerList = $this->Customer::where(function ($query) use ($searchValue) {
            $query->where('CustomerName', 'like', "%$searchValue%")
                ->orWhere('ContactName', 'like', "%$searchValue%");
        })->paginate(10);

        return view('admin.Customer.Search', ['Customer' => $CustomerList]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
