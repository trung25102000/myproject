<?php

namespace App\Http\Controllers;

use App\Models\Products;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function checkUsertype()
    {
        if(!Auth::user())
        {
            return redirect()->route('login');
        }
        if(Auth::user()->role=='admin')
        {
            return redirect()->route('admin.dashboard');
        }
        if(Auth::user()->role=='client')
        {
            return redirect()->route('client.dashboard');
        }
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }
}
