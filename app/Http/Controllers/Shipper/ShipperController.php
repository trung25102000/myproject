<?php

namespace App\Http\Controllers\Shipper;

use App\Http\Controllers\Controller;
use App\Models\Shipper;
use Illuminate\Http\Request;
use function PHPUnit\Framework\isEmpty;

class ShipperController extends Controller
{
    protected $Shipper;

    /**
     * @param $Shipper
     */
    public function __construct(Shipper $Shipper)
    {
        $this->Shipper = $Shipper;
    }


    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $shipperList=$this->Shipper->paginate(10);
        return view('admin.Shipper.Index',['Shipper'=>$shipperList]);
    }

    public function search(Request  $request)
    {
        $perPage = 10;

        $searchValue = $request->input('searchValue'); // từ khóa tìm kiếm
        $request->session()->put('searchValue', $searchValue);
        $shipperList = $this->Shipper::where(function ($query) use ($searchValue) {
            $query->where('ShipperName', 'like', "%$searchValue%")
                ->orWhere('Phone', 'like', "%$searchValue%");
        })->paginate(10);

        return view('admin.Shipper.Search', ['Shipper' => $shipperList]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('admin.Shipper.Create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'ShipperName' => 'required|max:50',
            'Phone' => 'required'
        ], [
            'ShipperName.required' => 'Tên người giao hàng không được bỏ trống',
            'ShipperName.max' => 'Tên người giao hàng không được vượt quá 50 kí tự',
            'Phone.required' => 'Số điện thoại không được bỏ trống'
        ]);

        // Xử lý lưu dữ liệu
        $dataRequest=$request->all();

        $Shipper=$this->Shipper->create($dataRequest);


        return redirect()->route('Shipper.index')->with('success', 'Thêm mới người giao hàng: '. $Shipper->ShipperName.' thành công');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $Shipper=$this->Shipper->findorfail($id);

        return view('admin.Shipper.Delete',compact('Shipper'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $Shipper=$this->Shipper->findorfail($id);
        return view('admin.Shipper.Edit',compact('Shipper'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $request->validate([
            'ShipperName' => 'required|max:50',
            'Phone' => 'required'
        ], [
            'ShipperName.required' => 'Tên người giao hàng không được bỏ trống',
            'ShipperName.max' => 'Tên người giao hàng không được vượt quá 50 kí tự',
            'Phone.required' => 'Số điện thoại không được bỏ trống'
        ]);

        $dataUpdate = $request->all();

        $Shipper = $this->Shipper->findOrFail($id);

        $Shipper->update($dataUpdate);

        return redirect()->route('Shipper.index')->with(['message' => 'Update  Shipper: '. $Shipper->ShipperName." success"]);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $Shipper=$this->Shipper->findorfail($id);

        $Shipper->delete($Shipper);

        return redirect()->route('Shipper.index')->with(['message' => 'Delete  Shipper: '.$Shipper->ShipperName.' success']);
    }
}
