<?php

namespace App\Http\Controllers\Supplier;

use App\Http\Controllers\Controller;
use App\Models\Suppliers;
use Illuminate\Http\Request;

class SupplierController extends Controller
{
    protected $Supplier;

    /**
     * @param $Supplier
     */
    public function __construct(Suppliers $Supplier)
    {
        $this->Supplier = $Supplier;
    }



    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $Supplier=$this->Supplier->latest('id')->paginate(10);

        return view('admin.Supplier.Index',compact('Supplier'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('admin.Supplier.Create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'SupplierName' => 'required|max:50',
            'PostalCode' => 'required',
            'Address' => 'required',
            'Phone' => 'required'
        ], [
            'SupplierName.required' => 'Tên nhà cung cấp không được bỏ trống',
            'SupplierName.max' => 'Tên nhà cung cấp không được vượt quá 50 kí tự',
            'PostalCode.required' => 'Mã bưu chính không được bỏ trống',
            'Address.required' => 'Địa chỉ không được bỏ trống',
            'Phone.required' => 'Số điện thoại không được bỏ trống'
        ]);

        // Xử lý lưu dữ liệu
        $dataRequest=$request->all();

        $Supplier=$this->Supplier->create($dataRequest);


        return redirect()->route('Supplier.index')->with('success', 'Thêm mới loại hàng thành công');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $Supplier=$this->Supplier->findorfail($id);

        return view('admin.Supplier.Delete',compact('Supplier'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $Supplier=$this->Supplier->findorfail($id);
        return view('admin.Supplier.Edit',compact('Supplier'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $request->validate([
            'SupplierName' => 'required|max:50',
            'PostalCode' => 'required',
            'Address' => 'required',
            'Phone' => 'required'
        ], [
            'SupplierName.required' => 'Tên nhà cung cấp không được bỏ trống',
            'SupplierName.max' => 'Tên nhà cung cấp không được vượt quá 50 kí tự',
            'PostalCode.required' => 'Mã bưu chính không được bỏ trống',
            'Address.required' => 'Địa chỉ không được bỏ trống',
            'Phone.required' => 'Số điện thoại không được bỏ trống'
        ]);

        $dataUpdate = $request->all();

        $Supplier = $this->Supplier->findOrFail($id);

        $Supplier->update($dataUpdate);

        return redirect()->route('Supplier.index')->with(['message' => 'Update  Supplier: '. $Supplier->SupplierName." success"]);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $Supplier=$this->Supplier->findorfail($id);

        $Supplier->delete($Supplier);

        return redirect()->route('Supplier.index')->with(['message' => 'Delete  Supplier: '.$Supplier->SupplierName.' success']);
    }

    /**
     *
    */
    public function search(Request $request)
    {
        $searchValue = $request->input('searchValue');

        $Supplier = $this->Supplier
            ->where('SupplierName', 'like', "%$searchValue%")
            ->orWhere('ContactName', 'like', "%$searchValue%")
            ->orWhere('Address', 'like', "%$searchValue%")
            ->orWhere('Phone', 'like', "%$searchValue%")
            ->orWhere('Country', 'like', "%$searchValue%")
            ->orWhere('City', 'like', "%$searchValue%")
            ->orWhere('PostalCode', 'like', "%$searchValue%")
            ->latest('id')
            ->paginate(10);

        return view('admin.Supplier.Index', compact('Supplier'));
    }
}
