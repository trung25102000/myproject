<?php

namespace App\Http\Controllers\Category;

use App\Http\Controllers\Controller;
use App\Models\categories;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    protected $category;

    /**
     * @param $category
     */
    public function __construct(categories $category)
    {
        $this->category = $category;
    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $categories = $this->category->latest('id')->paginate(10);

        return view('admin.categories.index', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('admin.categories.Create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'CategoryName' => 'required|max:50',
            'Description' => 'required',
            'ParentCategoryID' => 'nullable|numeric'
        ], [
            'CategoryName.required' => 'Tên loại hàng không được bỏ trống',
            'CategoryName.max' => 'Tên loại hàng không được vượt quá 50 kí tự',
            'Description.required' => 'Mô tả không được bỏ trống',
            'ParentCategoryID.numeric' => 'Mã loại hàng gốc phải là số'
        ]);

        // Xử lý lưu dữ liệu
        $dataRequest=$request->all();

        $Category=$this->category->create($dataRequest);


        return redirect()->route('Category.index')->with('success', 'Thêm mới loại hàng thành công');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $Category=$this->category->findorfail($id);

        return view('admin.categories.Delete',compact('Category'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($categoriesID)
    {

        $Category=$this->category->findorfail($categoriesID);
        return view('admin.categories.Edit',compact('Category'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request,$id)
    {
        $request->validate([
            'CategoryName' => 'required|max:50',
            'Description' => 'required',
            'ParentCategoryID' => 'nullable|numeric'
        ], [
            'CategoryName.required' => 'Tên loại hàng không được bỏ trống',
            'CategoryName.max' => 'Tên loại hàng không được vượt quá 50 kí tự',
            'Description.required' => 'Mô tả không được bỏ trống',
            'ParentCategoryID.numeric' => 'Mã loại hàng gốc phải là số'
        ]);

        $dataUpdate = $request->all();

        $category = $this->category->findOrFail($id);

        $category->update($dataUpdate);

        return redirect()->route('Category.index')->with(['message' => 'Update  category: '. $category->CategoryName." success"]);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $Category=$this->category->findorfail($id);

        $Category->delete($Category);

        return redirect()->route('Category.index')->with(['message' => 'Delete  category: '.$Category->CategoryName.' success']);
    }
    /**
     * Tìm kiếm theo Key
    */
    public function search(Request $request)
    {
        $searchValue = $request->input('searchValue');

        $categories = $this->category
            ->where('CategoryName', 'like', "%$searchValue%")
            ->orWhere('Description', 'like', "%$searchValue%")
            ->latest('id')
            ->paginate(10);

        return view('admin.categories.index', compact('categories'));
    }
}
