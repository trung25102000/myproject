<?php

namespace App\Http\Controllers\client\Product;

use App\Http\Controllers\Controller;
use App\Models\categories;
use App\Models\ProductAttributes;
use App\Models\ProductPhotos;
use App\Models\Products;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    protected $product;

    /**
     * @param $product
     */
    public function __construct(Products $product)
    {
        $this->product = $product;
    }


    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $products=$this->product->paginate(10);

        $categories=categories::all();
        $product=$this->product->findOrFail($id);
        $images = ProductPhotos::where('products_id', $product->id)->get();
        $Attributes = ProductAttributes::where('products_id', $product->id)->get();
        return view('client.DashBoard.detail',compact('product','categories','products','images','Attributes'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }

    public function Search(Request $request)
    {
        $perPage = 10;
        $CategoryID=$request->input('CategoryID');
        $selectedCategoryID = $request->input('CategoryID');
        $searchValue = $request->input('searchValue'); // từ khóa tìm kiếm
        $request->session()->put('searchValue', $searchValue);
        $productlist = $this->product::where(function ($query) use ($searchValue,$CategoryID) {
            if(!empty($CategoryID))
            {
                $query->where('categories_id', 'like', "%$CategoryID%");
            }
            if(!empty($searchValue)){
                $query->where('ProductName', 'like', "%$searchValue%")
                    ->orWhere('Price', 'like', "%$searchValue%")
                    ->orWhere('Unit', 'like', "%$searchValue%");
            }
        })->paginate(10);

        return view('client.DashBoard.search', ['products' => $productlist])->with('selectedCategoryID', $selectedCategoryID);
    }

    public function AddToCart($productId)
    {

        redirect()->back()->with('message','Them vao gio hang thanh cong!');
    }
}
