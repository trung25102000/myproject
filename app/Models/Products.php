<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
    use HasFactory;
    protected $table='products';
    protected $fillable=[
        'suppliers_id',
        'categories_id',
        'ProductName',
        'Unit',
        'Price',
        'Photo'
    ];


}
