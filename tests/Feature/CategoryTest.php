<?php

namespace Tests\Feature;

use App\Models\categories;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;
use Illuminate\Foundation\Testing\Concerns\InteractsWithDatabase;

class CategoryTest extends TestCase
{

    protected $user;
    protected $category;
    use InteractsWithDatabase;

    protected function setUp(): void
    {
        parent::setUp();

        // lấy một user và đăng nhập từ User model
        $this->user = User::first();
        $this->actingAs($this->user);

        // lấy một category từ model
        $this->category = categories::first();
    }
    public function testCategoryIndexPageDisplaysListOfCategories()
    {

        $response = $this->get(route('Category.index'));

        // dòng status muốn mang client nhân đc
        $response->assertStatus(200);

        // view mà reponse mong muốn tra về dc
       $response->assertViewIs('admin.categories.index');
        $response->assertViewHas('categories');
       $viewCategories = $response->viewData('categories');
      $this->assertCount(10, $viewCategories);
        $this->assertInstanceOf(categories::class, $viewCategories->first());
    }

    /** @test */
    public function it_can_show_the_create_form_for_category()
    {
        $response = $this->get(route('Category.create'));

        $response->assertOk();
        $response->assertViewIs('admin.categories.Create');
    }

    /** @test */
    public function it_can_create_a_new_category()
    {
        $categoryData = categories::factory()->raw();

        $response = $this->post(route('Category.store'), $categoryData);

        $this->assertDatabaseHas('categories', $categoryData);
        $response->assertRedirect(route('Category.index'));
    }

    /** @test */
    public function it_can_show_the_edit_form_for_category()
    {
        $response = $this->get(route('Category.edit', $this->category->id));

        $response->assertOk();
        $response->assertViewIs('admin.categories.Edit');
        $response->assertViewHas('Category', $this->category);
    }

    /** @test */
    public function it_can_update_an_existing_category()
    {
        $newCategoryData = categories::factory()->raw();

        $response = $this->put(route('Category.update', $this->category->id), $newCategoryData);

        $this->assertDatabaseHas('categories', $newCategoryData);
        $response->assertRedirect(route('Category.index'));
    }

    /** @test */
    public function it_can_show_the_delete_form_for_category()
    {
        $response = $this->get(route('Category.show', $this->category->id));

        $response->assertOk();
        $response->assertViewIs('admin.categories.Delete');
        $response->assertViewHas('Category', $this->category);
    }

    /** @test */
    public function it_can_delete_an_existing_category()
    {
        // Tạo một category
        $category = categories::factory()->create();

        // Gửi request DELETE đến route xóa category vừa tạo
        $response = $this->delete(route('Category.destroy', $category->id));

        // Kiểm tra redirect đến route 'Category.index'
        $response->assertRedirect(route('Category.index'));

        // Kiểm tra session có message là 'Delete  category: {CategoryName} success'
        $response->assertSessionHas('message', 'Delete  category: ' . $category->CategoryName . ' success');

        // Kiểm tra rằng category đã bị xóa khỏi cơ sở dữ liệu
        $this->assertDatabaseMissing('categories', ['id' => $category->id]);
    }

    /** @test */
    public function it_can_search_for_categories_by_name_or_description()
    {
        $categoryOne = categories::factory()->create([
            'CategoryName' => 'Category One',
            'Description' => 'This is the first category'
        ]);
        $categoryTwo = categories::factory()->create([
            'CategoryName' => 'Category Two',
            'Description' => 'This is the second category'
        ]);

        $response = $this->get(route('category.search'), ['searchValue' => 'second']);

        $response->assertOk();
        $response->assertViewIs('admin.categories.index');
        $response->assertSee($categoryTwo->CategoryName);
       // $response->assertDontSee($categoryOne->CategoryName);
    }
    /** @test */
    public function it_returns_not_found_if_search_value_is_not_provided()
    {
        $categoryOne = categories::factory()->create([
            'CategoryName' => 'Category One',
            'Description' => 'This is the first category'
        ]);
        $categoryTwo = categories::factory()->create([
            'CategoryName' => 'Category Two',
            'Description' => 'This is the second category'
        ]);
        $response = $this->get(route('category.search'),['searchValue' => '@@']);

        //$response->assertStatus(404);
        $response->assertViewHas('categories', null);
    }


}
