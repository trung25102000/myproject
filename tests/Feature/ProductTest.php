<?php

namespace Tests\Feature;

use App\Models\categories;
use App\Models\Products;
use App\Models\Suppliers;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ProductTest extends TestCase
{
    protected $user;
    protected $product;
    protected function setUp(): void
    {
        parent::setUp();

        // lấy một user và đăng nhập từ User model
        $this->user = User::first();
        $this->actingAs($this->user);
        $this->product=Products::first();

    }
    /**
     * A basic feature test example.
     */
    public function test_example(): void
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }
    /** @test */
    public function it_can_show_form_create()
    {
        $response=$this->get(route('Product.create'));

        $response->assertStatus(200);

        $response->assertViewIs('admin.Product.Create');

        $response->assertViewHas('Category');
        $response->assertViewHas('Supplier');
        $ViewCategory=$response->viewData('Category');
        $this->assertInstanceOf(categories::class,$ViewCategory->first());

        $ViewSupplier=$response->viewData('Supplier');
        $this->assertInstanceOf(Suppliers::class,$ViewSupplier->first());
    }
}
