<?php

namespace Tests\Feature;

use App\Models\categories;
use App\Models\Suppliers;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class SupplierTest extends TestCase
{
    protected $user;
    protected $Supplier;
    protected function setUp(): void
    {
        parent::setUp();

        // lấy một user và đăng nhập từ User model
        $this->user = User::first();
        $this->actingAs($this->user);

        // lấy một category từ model
        $this->Supplier = Suppliers::first();
    }

    /**
     * A basic feature test example.
     */
    public function test_example(): void
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }

    /**@test **/
    public function testSupplierIndexPageDisplaysListOfSuppliers(){
        $Supplier=Suppliers::all();

        $response=$this->get(route('Supplier.index',compact('Supplier')));

        $response->assertStatus(200);

        $response->assertViewIs('admin.Supplier.Index');

        $response->assertViewHas('Supplier');

        $this->assertNotEmpty($response->original->getData()['Supplier']);

    }
}
