<?php

use Illuminate\Support\Facades\Route;
use App\Models\Products;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Auth::routes();
Route::get('/', [App\Http\Controllers\HomeController::class, 'checkUsertype'])->name('home');

Route::get('/admin/dashboard', function () {
    return view('admin.DashBoard.index');
})->middleware('auth')->name('admin.dashboard');
Route::get('/client/dashboard', function () {
    $categories=\App\Models\categories::all();
    $products=Products::paginate(10);
    return view('client.DashBoard.index',compact('products','categories'));
})->middleware('auth')->name('client.dashboard');



Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::resource('Category',controller: \App\Http\Controllers\Category\CategoryController::class);
Route::get('/category', '\App\Http\Controllers\Category\CategoryController@index');

Route::get('/category/search', '\App\Http\Controllers\Category\CategoryController@search')->name('category.search');

Route::resource('Supplier',controller: \App\Http\Controllers\Supplier\SupplierController::class);
Route::resource('Product',controller: \App\Http\Controllers\Product\ProductController::class);
Route::get('/supplier/search', '\App\Http\Controllers\Supplier\SupplierController@search')->name('Supplier.search');
Route::get('/product/search', '\App\Http\Controllers\Product\ProductController@search')->name('Product.search');
Route::resource('Shipper',\App\Http\Controllers\Shipper\ShipperController::class);
Route::POST('/shipper/search', '\App\Http\Controllers\Shipper\ShipperController@search')->name('Shipper.search');
Route::resource('Customer',controller: \App\Http\Controllers\Customer\CustomerController::class);
Route::resource('Employee',controller: \App\Http\Controllers\Employee\EmployeeController::class);
Route::POST('/customer/search', '\App\Http\Controllers\Customer\CustomerController@search')->name('Customer.search');
Route::POST('/employee/search', '\App\Http\Controllers\Employee\EmployeeController@search')->name('Employee.search');
Route::resource('Order',\App\Http\Controllers\Order\OrderController::class);
Route::POST('/Order/search', '\App\Http\Controllers\Order\OrderController@search')->name('Order.search');
Route::POST('/Order/searchproduct', [\App\Http\Controllers\Order\OrderController::class, 'searchproduct'])->name('Order.searchproduct');
Route::POST('/Order/AddToCart', [\App\Http\Controllers\Order\OrderController::class, 'AddToCart'])->name('Order.AddToCart');
Route::GET('/Order/removeFromCart/{productId}', [\App\Http\Controllers\Order\OrderController::class, 'removeFromCart'])->name('Order.removeFromCart');
Route::GET('/Order/RemoveCart/{productId}', [\App\Http\Controllers\Order\OrderController::class, 'RemoveCart'])->name('Order.RemoveCart');
Route::POST('/Order/Init', [\App\Http\Controllers\Order\OrderController::class, 'Init'])->name('Order.Init');
Route::GET('/Order/RemoveDetail/{productId}/{orderid}', [\App\Http\Controllers\Order\OrderController::class, 'RemoveDetail'])->name('Order.RemoveDetail');
Route::GET('/Order/Details/{OrderId}', [\App\Http\Controllers\Order\OrderController::class, 'Details'])->name('Order.Details');
Route::GET('/Order/Accept/{OrderId}', [\App\Http\Controllers\Order\OrderController::class, 'Accept'])->name('Order.Accept');
Route::GET('/Order/Shipping/{OrderId}', [\App\Http\Controllers\Order\OrderController::class, 'Shipping'])->name('Order.Shipping');
Route::POST('/Order/Shipped', [\App\Http\Controllers\Order\OrderController::class, 'Shipped'])->name('Order.Shipped');
Route::GET('/Order/Finished/{OrderId}', [\App\Http\Controllers\Order\OrderController::class, 'Finished'])->name('Order.Finished');
Route::GET('/Order/Cancel/{OrderId}', [\App\Http\Controllers\Order\OrderController::class, 'Cancel'])->name('Order.Cancel');
Route::GET('/Order/Reject/{OrderId}', [\App\Http\Controllers\Order\OrderController::class, 'Reject'])->name('Order.Reject');
Route::POST('/Client/Product/Search',[\App\Http\Controllers\client\Product\ProductController::class,'search'])->name('Client.Product.Search');
Route::GET('/Client/Product/Details/{ProductID}', [\App\Http\Controllers\client\Product\ProductController::class, 'show'])->name('Client.Product.Details');
Route::GET('/Client/Product/AddToCart/{ProductID}', [\App\Http\Controllers\client\Product\ProductController::class, 'AddToCart'])->name('Client.Product.AddToCart');
