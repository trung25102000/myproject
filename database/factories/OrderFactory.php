<?php

namespace Database\Factories;

use App\Models\Customer;
use App\Models\Employees;
use App\Models\Shipper;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Order>
 */
class OrderFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $status = ['Đơn hàng mới (chờ duyệt)!', 'Đơn hàng đã duyệt (chờ chuyển hàng)!', 'Đơn hàng đang được giao','Đơn hàng đã hoàn tất thành công','Đơn hàng bị hủy','Đơn hàng bị từ chối'];
        $Employee=Employees::all()->random();
        $Customer=Customer::all()->random();
        $Shipper=Shipper::all()->random();
        return [
            'employees_id'=>$Employee->id,
            'customer_id'=>$Customer->id,
            'shipper_id'=>$Shipper->id,
            'OrderTime'=>$this->faker->dateTime(),
            'AcceptTime'=>$this->faker->dateTime(),
            'ShippedTime'=>$this->faker->dateTime(),
            'FinishedTime'=>$this->faker->dateTime(),
            'Status' => $this->faker->randomElement($status)
        ];
    }
}
