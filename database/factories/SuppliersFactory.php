<?php

namespace Database\Factories;

use App\Models\Suppliers;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Foundation\Testing\WithFaker;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Suppliers>
 */
class SuppliersFactory extends Factory
{
    use WithFaker;
    protected $model=Suppliers::class;
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'SupplierName'=>$this->faker->name(),
            'ContactName'=>$this->faker->name(),
            'Address'=>$this->faker->address(),
            'City'=>$this->faker->city(),
            'PostalCode'=>$this->faker->text(10),
            'Country'=>$this->faker->country(),
            'Phone'=>$this->faker->phoneNumber()
        ];
    }
}
