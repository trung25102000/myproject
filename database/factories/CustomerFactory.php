<?php

namespace Database\Factories;

use App\Models\customer;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Foundation\Testing\WithFaker;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\customer>
 */
class CustomerFactory extends Factory
{
    use WithFaker;

    protected $model=customer::class;
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'CustomerName'=>$this->faker->name(15),
            'ContactName'=>$this->faker->name(20),
            'Address'=>$this->faker->address(),
            'City'=>$this->faker->city(),
            'PostalCode'=>$this->faker->postcode(),
            'Country'=>$this->faker->country(),
            'Email'=>$this->faker->email(),
            'Password'=>$this->faker->text(8)
        ];
    }
}
