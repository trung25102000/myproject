<?php

namespace Database\Factories;

use App\Models\Order;
use App\Models\OrderDetails;
use App\Models\Products;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Foundation\Testing\WithFaker;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\OrderDetails>
 */
class OrderDetailsFactory extends Factory
{
    use WithFaker;
    protected $model=OrderDetails::class;
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $Product=Products::all()->random();
        $Order=Order::all()->random();
        return [
            'products_id'=>$Product->id,
            'order_id'=>$Order->id,
            'Quantity'=>$this->faker->numberBetween(1,10),
            'SalePrice'=>$this->faker->randomFloat(2, 10000, 1000000), // tạo giá trong khoảng từ 10.000 đến 1.000.000 đồng, với tối đa 2 chữ số thập phân
        ];
    }
}
