<?php

namespace Database\Factories;

use App\Models\ProductPhotos;
use App\Models\Products;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Foundation\Testing\WithFaker;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\ProductPhotos>
 */
class ProductPhotosFactory extends Factory
{
    use WithFaker;
    protected $model=ProductPhotos::class;
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $product=Products::all()->random();
        return [
            'products_id'=>$product->id,
            'Photo'=>$this->faker->image('public/Images', 640, 480, 'food', true),
            'Description'=>$this->faker->text,
            'DisplayOrder'=>$this->faker->numberBetween(1,100),
            'IsHidden'=>$this->faker->boolean()
        ];
    }
}
