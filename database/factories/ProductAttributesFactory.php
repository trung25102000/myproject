<?php

namespace Database\Factories;

use App\Models\Products;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\ProductAttributes>
 */
class ProductAttributesFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $Product=Products::all()->random();
        return [
            'products_id'=>$Product->id,
            'AttributeName'=>$this->faker->name(50),
            'AttributeValue'=>$this->faker->text(30),
            'DisplayOrder'=>$this->faker->numberBetween(1,100)
        ];
    }
}
