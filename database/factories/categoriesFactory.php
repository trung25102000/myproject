<?php

namespace Database\Factories;

use App\Models\categories;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Foundation\Testing\WithFaker;
use function Symfony\Component\Translation\t;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\categories>
 */
class categoriesFactory extends Factory
{
    use WithFaker;
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    protected $model=categories::class;

    public function definition(): array
    {
        return [
            'CategoryName'=>$this->faker->name(),
            'Description'=>$this->faker->text(50),
            'ParentCategoryID'=>$this->faker->numerify()
        ];
    }
}
