<?php

namespace Database\Factories;

use App\Models\categories;
use App\Models\Products;
use App\Models\Suppliers;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Foundation\Testing\WithFaker;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Products>
 */
class ProductsFactory extends Factory
{
    use WithFaker;
    protected $model=Products::class;


    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $supplier=Suppliers::all()->random();
        $category=categories::all()->random();
        return [
            'suppliers_id'=>$supplier->id,
            'categories_id'=>$category->id,
            'ProductName'=>$this->faker->name(),
            'Unit'=>$this->faker->text(10),
            'Price'=>$this->faker->randomFloat(2, 10000, 1000000), // tạo giá trong khoảng từ 10.000 đến 1.000.000 đồng, với tối đa 2 chữ số thập phân
            'Photo'=>$this->faker->image('public/Images', 640, 480, 'food', true)
        ];
    }
}
