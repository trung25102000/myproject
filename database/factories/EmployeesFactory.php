<?php

namespace Database\Factories;

use App\Models\Employees;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Foundation\Testing\WithFaker;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Employees>
 */
class EmployeesFactory extends Factory
{
    Use WithFaker;
    protected $model=Employees::class;
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'FullName'=>$this->faker->name(20),
            'BirthDate'=>$this->faker->dateTime(),
            'Photo'=>$this->faker->image('public/Images', 640, 480, 'Employee', true),
            'Notes'=>$this->faker->text(30),
            'Email'=>$this->faker->email(),
            'Password'=>$this->faker->text(7)
        ];
    }
}

