<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    protected $model=Orders::class;
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->foreignIdFor(\App\Models\Customer::class)->constrained()->cascadeOnDelete();
            $table->foreignIdFor(\App\Models\Employees::class)->constrained()->cascadeOnDelete();
            $table->foreignIdFor(\App\Models\Shipper::class)->nullable()->constrained()->cascadeOnDelete()->nullOnDelete();
            $table->dateTime('OrderTime')->nullable();
            $table->dateTime('AcceptTime')->nullable();
            $table->dateTime('ShippedTime')->nullable();
            $table->dateTime('FinishedTime')->nullable();
            $table->string('Status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('orders');
    }
};
