@extends('admin.layouts.app')
@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            CATEGORY
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{route('admin.dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">CATEGORY</li>
        </ol>
        @if(session('success'))
            <div class="alert alert-success">
                {{ session('success') }}
            </div>
        @endif
        @if(session('message'))
            <div class="alert alert-success">
                {{ session('message') }}
            </div>
        @endif
    </section>

    <!-- Main content -->
    <section class="content">
        <div class=" box box-primary">
            <div class="box-body">
                <form id="formSearch" action="{{ route('category.search') }}" method="get">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Search" name="searchValue" autofocus
                               value="{{old('searchValue')}}">
                        <div class="input-group-btn">
                            <button class="btn btn-default" type="submit">
                                Tìm Kiếm
                                <i class="glyphicon glyphicon-search"></i>
                            </button>
                            <a href="{{route('Category.create')}}" class=" btn btn-primary" style="margin-left:3px">
                                <i class="glyphicon glyphicon-plus"></i>
                                Bổ sung
                            </a>
                        </div>
                    </div>
                </form>
                <div>
                    <p>Có <strong> {{$categories->total()}} </strong> kết quả trong tổng số
                        <strong>{{ $categories->lastPage() }}</strong> trang.
                    </p>
                    <table class="table table-hover table-bordered ">
                        <thead class="bg-blue-gradient ">
                        <th style="width:10px">STT</th>
                        <th>Tên loại hàng</th>
                        <th>Mô tả</th>
                        <th>Mã danh mục gốc</th>
                        <th style="width:130px">Số sản phẩm</th>
                        <th style="width:100px"></th>
                        </thead>
                        <tbody>

                        @foreach ($categories as $items)
                            <tr>
                                <td>{{ $loop->index+1 }}</td>
                                <td>{{$items->CategoryName}}</td>
                                <td>{{$items->Description}}</td>
                                <td>{{$items->ParentCategoryID}}</td>
                                <td>{{\App\Models\Products::where('categories_id', $items->id)->count()}}</td>
                                <td>
                                    <a href="{{ route('Category.edit',$items->id)}}" class="btn btn-success btn-sm sm"><i  class="glyphicon glyphicon-edit"></i></a>
                                    <a href="{{ route('Category.show',$items->id)}}" class="btn btn-danger btn-sm sm"><i
                                            class="glyphicon glyphicon-trash"></i></a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                    <div class="text-center">
                        <nav aria-label="Page navigation example">

                        </nav>
                    </div>
                </div>


            </div>
        </div>

    </section>
    <!-- /.content -->

    <!-- phân trang -->
    <div class="text-center">
        <nav aria-label="Page navigation example">
            <ul class="pagination">
                <li class="page-item">
        @if ($categories->currentPage() > 1)
            <a href="{{ $categories->previousPageUrl() }}" class="page-link">&laquo; Previous</a>
        @endif

        @for ($i = 1; $i <= $categories->lastPage(); $i++)
            <a href="{{ $categories->url($i) }}" class="page-link">{{ $i }}</a>
        @endfor

        @if ($categories->currentPage() < $categories->lastPage())
            <a href="{{ $categories->nextPageUrl() }}" class="page-link">Next &raquo;</a>
        @endif
                </li>
            </ul>
        </nav>
    </div>

@endsection


