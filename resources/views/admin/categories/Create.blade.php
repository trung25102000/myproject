@extends('admin.layouts.app')
@section('content')
<div class="box box-primary">
    <div class="box-body">
        @if($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <form class="form-horizontal" action="{{ route('Category.store') }}" method="post">
            @csrf
            <div class="form-group">
                <label class="control-label col-sm-2">Tên loại hàng:</label>
                <div class="col-sm-10">
                    <input class="form-control" autofocus name="CategoryName"  required>
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-sm-2">Mô tả:</label>
                <div class="col-sm-10">
                    <input class="form-control"  name="Description" ">
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-sm-2">Mã loại hàng gốc:</label>
                <div class="col-sm-10">
                    <input class="form-control"  name="ParentCategoryID" " required>
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-default bg-blue">
                        <i class="glyphicon glyphicon-floppy-disk"></i>
                        Lưu dữ liệu
                    </button>
                    <a href="/Category" class="btn btn-warning">quay lại</a>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection
