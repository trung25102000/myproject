@if($Employee->total()>0)
    <p>Có <strong>{{$Employee->total()}}</strong> nhà cung cấp trong tổng số <strong>{{$Employee->lastPage()}}</strong> trang. </p>
    <div style="display:flex;flex-wrap:wrap; justify-content:center">
        @foreach($Employee as $item)
            <div class="table-hover" style="
                            position:relative;
                            box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
                            transition: 0.3s;
                            border-radius: 10px;
                            width:350px;
                            margin-bottom:10px;
                            margin-right:20px;">
                <div style="position: absolute; right: 10px; top:10px">

                    <img src="{{asset(str_replace("public/", "", $item->Photo))}}"
                         style="width: 100px;
                                border-radius: 10px;
                                height: 100px;
                                object-fit: cover; ">

                </div>
                <div class="card-body ">
                    <div class="form-group">
                        <p style="margin: 5px; font-family: 'cursive' ">
                            <strong>Họ:</strong> {{$item->FullName}}
                        </p>
                    </div>
                    <div class="form-group">
                        <p style="margin: 5px; font-family: 'cursive' ">
                            <strong>Tên:</strong> {{$item->FullName}}
                        </p>
                    </div>
                    <div class="form-group">
                        <p style="margin: 5px; font-family: 'cursive' ">
                            <strong>Email:</strong> {{$item->Email}}
                        </p>
                    </div>

                    <div class="form-group">
                        <p style="margin-top: 60px; margin-left: 5px; margin-bottom: -10px; font-family: 'cursive';
                       display: -webkit-box;
                                           -webkit-line-clamp: 3;
                                           -webkit-box-orient: vertical;
                                           overflow: hidden;
                                           height:60px;
">
                            "{{$item->Notes}}"
                        </p>
                    </div>

                    <div style="margin-left:10px; margin-top:5px; text-align:center; margin-bottom:5px">
                        <a href="{{route('Employee.edit',$item->id)}}" class=" btn btn-success  " style="padding:2px">
                            <i class="glyphicon glyphicon-edit"></i>
                            Sửa
                        </a>
                        <a href="{{route('Employee.show',$item->id)}}" class=" btn btn-danger " style="padding:2px; margin-left:2px;">
                            <i class="glyphicon glyphicon-trash"></i>
                            Xóa
                        </a>
                    </div>

                </div>
            </div>
        @endforeach
    </div>
    <!-- phân trang -->
    <div class="text-center">
        <nav aria-label="Page navigation example">
            <ul class="pagination">
                <li class="page-item">
                    @if ($Employee->currentPage() > 1)
                        <a href="{{ $Employee->previousPageUrl() }}" class="page-link">&laquo; Previous</a>
                    @endif

                    @for ($i = 1; $i <= $Employee->lastPage(); $i++)
                        <a href="{{ $Employee->url($i) }}" class="page-link">{{ $i }}</a>
                    @endfor

                    @if ($Employee->currentPage() < $Employee->lastPage())
                        <a href="{{ $Employee->nextPageUrl() }}" class="page-link">Next &raquo;</a>
                    @endif
                </li>
            </ul>
        </nav>
    </div>
@else
    <h2>Không tìm thấy dữ liệu muốn tìm</h2>
@endif
