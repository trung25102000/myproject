@if($Customer->total()>0)
    <p>Có <strong>{{$Customer->total()}}</strong> nhà cung cấp trong tổng số <strong>{{$Customer->lastPage()}}</strong> trang. </p>

    <table class="table table-hover table-bordered ">
        <thead class="bg-blue-gradient ">
        <th>STT</th>
        <th>Tên khách hàng</th>
        <th>Tên liên lạc</th>
        <th>Địa chỉ</th>
        <th>Email</th>
        <th>Quốc gia</th>
        <th>Thành phố </th>
        <th>Mã bưu chính</th>
        <th></th>
        </thead>
        <tbody>
        @foreach($Customer as $item)
            <tr>
                <td>{{ $loop->index+1 }}</td>
                <td>{{$item->CustomerName}}}</td>
                <td>{{$item->ContactName}}</td>
                <td>{{$item->Address}}</td>
                <td>{{$item->Email}}</td>
                <td>{{$item->Country}}</td>
                <td>{{$item->City}}</td>
                <td>{{$item->PostalCode}}</td>
                <td >
                    <a href="{{route('Customer.edit',$item->id)}}" class="btn btn-success btn-sm "><i class="glyphicon glyphicon-edit"></i> </a>
                    <a href="{{route('Customer.show',$item->id)}}" class="btn btn-danger btn-sm "><i class="glyphicon glyphicon-trash"></i></a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <!-- phân trang -->
    <div class="text-center">
        <nav aria-label="Page navigation example">
            <ul class="pagination">
                <li class="page-item">
                    @if ($Customer->currentPage() > 1)
                        <a href="{{ $Customer->previousPageUrl() }}" class="page-link">&laquo; Previous</a>
                    @endif

                    @for ($i = 1; $i <= $Customer->lastPage(); $i++)
                        <a href="{{ $Customer->url($i) }}" class="page-link">{{ $i }}</a>
                    @endfor

                    @if ($Customer->currentPage() < $Customer->lastPage())
                        <a href="{{ $Customer->nextPageUrl() }}" class="page-link">Next &raquo;</a>
                    @endif
                </li>
            </ul>
        </nav>
    </div>
@else
    <h2>Không tìm thấy dữ liệu muốn tìm</h2>
@endif
