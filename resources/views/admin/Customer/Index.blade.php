@extends('admin.layouts.app')
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Customer
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{route('admin.dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Customer</li>
        </ol>
        @if(session('success'))
            <div class="alert alert-success">
                {{ session('success') }}
            </div>
        @endif
        @if(session('message'))
            <div class="alert alert-success">
                {{ session('message') }}
            </div>
        @endif
    </section>
    <div class=" box box-primary">
        <div class="box-body">
            <form id="formSearch" action="#" method="POST">
                @csrf
                <div class="input-group">
                    <input type="text" class="form-control list" placeholder="Search" name="searchValue" autofocus
                           value="{{old('searchValue')}}">
                    <div class="input-group-btn">
                        <button class="btn btn-default" type="submit">
                            Tìm Kiếm
                            <i class="glyphicon glyphicon-search"></i>
                        </button>
                        <a href="{{route('Customer.create')}}" class=" btn btn-primary" style="margin-left:3px">
                            <i class="glyphicon glyphicon-plus"></i>
                            Bổ sung
                        </a>
                    </div>
                </div>
            </form>
            <div id="CustomerList">
            </div>
        </div>
        <script type="text/javascript">
            $(document).ready(function(){
                // Sử dụng AJAX để tìm kiếm và phân trang
                $("#formSearch").keyup(function (e) {
                    console.log('Hello World!');
                    e.preventDefault();
                    doSearch(1);
                    return;
                });
                $("#formSearch").submit(function (e) {
                    console.log('Hello World!');
                    e.preventDefault();
                    doSearch(1);
                    return;
                });
                doSearch({{$Customer->currentPage()}});
                // Hàm thực hiện tìm kiếm và phân trang bằng AJAX
                function doSearch(page) {
                    var url = "{{route('Customer.search')}}";
                    var postData = $("#formSearch").serializeArray();
                    postData.push({ "name": "page", "value": page });
                    $.ajax({
                        url: url,
                        type: "POST",
                        data: postData,
                        dataType: "html",
                        error: function () {
                            alert("Your request is not valid!");
                        },
                        success: function (data) {
                            $("#CustomerList").html(data);
                            $(".page-link").click(function(e) {
                                e.preventDefault();
                                var page = $(this).attr('href').split('page=')[1];
                                doSearch(page);
                            });
                        }
                    });
                }
            });
        </script>
    </div>
@endsection




