﻿@extends('admin.layouts.app')
@section('content')
    @if($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
<div class="box box-primary">
    <div class="box-header with-border ">
        <h3 class="box-title">Thông tin mặt hàng</h3>
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse">
                <i class="fa fa-minus"></i>
            </button>
        </div>
    </div>

    <div class="box-body">
        <form class="form-horizontal" action="{{ route('Product.update',$Product->id) }}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <input type="hidden" name="id" value="{{$Product->id}}">
            <div class="form-group">
                <label class="control-label col-sm-2">Tên mặt hàng:</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="ProductName" id="ProductName" autofocus value="{{$Product->ProductName}}">
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-sm-2">Loại hàng:</label>
                <div class="col-sm-10">
                    <select class="form-control" id="Category" name="categories_id">
                        <option>--Chọn Loại Hàng--</option>
                        @foreach($Category as $items)
                            @if($items->id==$Product->categories_id)
                                <option value="{{$items->id}}" selected>{{$items->CategoryName}}</option>
                            @else
                                <option value="{{$items->id}}">{{$items->CategoryName}}</option>
                            @endif
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-sm-2">Nhà cung cấp:</label>
                <div class="col-sm-10">
                    <select class="form-control" id="Supplier" name="suppliers_id">
                        <option>--Chọn Nhà Cung Cấp--</option>
                        @foreach($Supplier as $items)
                            @if($items->id==$Product->suppliers_id)
                                <option value="{{$items->id}}" selected>{{$items->SupplierName}}</option>
                            @else
                                <option value="{{$items->id}}">{{$items->SupplierName}}</option>
                            @endif
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-sm-2">Đơn vị tính:</label>
                <div class="col-sm-10">
                    <input class="form-control" name="Unit" id="Unit" value="{{$Product->Unit}}">
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-sm-2">Giá hàng:</label>
                <div class="col-sm-10">
                    <input class="form-control" name="Price" id="Price" value="{{$Product->Price}}">
                </div>
            </div>


            <div class="form-group">
                <label class="control-label col-sm-2">Ảnh minh họa:</label>
                <div class="col-sm-10">
                    <input type="file" class="form-control" name="UploadPhoto" onchange="document.getElementById('Photo').src = window.URL.createObjectURL(this.files[0])" />
                </div>
            </div>

            <div class="form-group">
                <div class="col-lg-offset-2 col-sm-10">
                    <img id="Photo" src="{{asset(str_replace("public/", "", $Product->Photo))}}" class="img img-bordered" style="width:200px" />
                </div>
            </div>
            <input type="hidden" name="Photo" value="{{$Product->Photo}}">


            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-primary">
                        <i class="fa fa-save"></i> Lưu dữ liệu
                    </button>
                    <a class="btn btn-warning" href="{{route('Product.index')}}">
                        <i class="fa fa-ban"></i> Quay lại
                    </a>
                </div>
            </div>
        </form>
    </div>

</div>

<div class="box box-info">
    <div class="box-header with-border ">
        <h3 class="box-title">Thư viện ảnh</h3>
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse">
                <i class="fa fa-minus"></i>
            </button>
        </div>
    </div>
    <div class="box-body">
        <table class="table table-bordered table-hover table-striped">
            <thead>
                <tr class="bg-gray">
                    <th>Ảnh</th>
                    <th>Mô tả/tiêu đề</th>
                    <th>Thứ tự hiển thị</th>
                    <th>Ẩn ảnh</th>
                    <th class="text-right">
                        <a class="btn btn-xs btn-primary" href="#">
                            <i class="fa fa-plus"></i>
                        </a>
                    </th>
                </tr>
            </thead>
            <tbody>
            @foreach($ProductPhoto as $items)
                <tr>
                    <td style="vertical-align:middle">
                        <img src="{{asset(str_replace("public/", "", $items->Photo))}}" style="width:100px" />
                    </td>
                    <td style="vertical-align:middle">
                        {{$items->Description}}
                    </td>
                    <td style="vertical-align:middle">{{$items->DisplayOrder}}</td>
                    <td style="vertical-align:middle">{{$items->IsHidden}}</td>
                    <td style="vertical-align:middle;text-align:right">
                        <a class="btn btn-xs btn-primary" href="#">
                            <i class="fa fa-edit"></i>
                        </a>
                        <a class="btn btn-xs btn-danger" href="#" onclick="return confirm('Xóa ảnh của mặt hàng hay không?')">
                            <i class="fa fa-trash-o"></i>
                        </a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    <div class="box-footer text-center">
    </div>

</div>

<div class="box box-success">
    <div class="box-header with-border ">
        <h3 class="box-title">Thuộc tính của mặt hàng</h3>
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse">
                <i class="fa fa-minus"></i>
            </button>
        </div>
    </div>

    <div class="box-body">
        <table class="table table-bordered table-hover table-striped">
            <thead>
                <tr class="bg-gray">
                    <th>Tên thuộc tính</th>
                    <th>Giá trị thuộc tính</th>
                    <th>Thứ tự hiển thị</th>
                    <th class="text-right">
                        <a class="btn btn-xs btn-primary" href="~/product/attribute/add/@Model.Data.ProductID">
                            <i class="fa fa-plus"></i>
                        </a>
                    </th>
                </tr>
            </thead>
            <tbody>
                @foreach($ProductAttribute as $items)
                    <tr>
                        <td style="vertical-align:middle">{{$items->AttributeName}}</td>
                        <td style="vertical-align:middle">{{$items->AttributeValue}}</td>
                        <td style="vertical-align:middle">{{$items->DisplayOrder}}</td>
                        <td style="vertical-align:middle; text-align:right">
                            <a class="btn btn-xs btn-primary" href="#">
                                <i class="fa fa-edit"></i>
                            </a>
                            <a class="btn btn-xs btn-danger" href="#" onclick="return confirm('Xóa thuộc tính này của mặt hàng?')">
                                <i class="fa fa-trash-o"></i>
                            </a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <div class="box-footer text-center">
    </div>

</div>
@endsection
