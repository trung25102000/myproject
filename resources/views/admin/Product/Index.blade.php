﻿@extends('admin.layouts.app')
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            PRODUCT
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{route('admin.dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">PRODUCT</li>
        </ol>
        @if(session('success'))
            <div class="alert alert-success">
                {{ session('success') }}
            </div>
        @endif
        @if(session('message'))
            <div class="alert alert-success">
                {{ session('message') }}
            </div>
        @endif
    </section>

    <!-- Main content -->
    <section class="content">
        <div class=" box box-primary">
            <div class="box-body">
                <form id="formSearch" action="{{ route('Product.search') }}" method="get">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Search" name="searchValue" autofocus
                               value="{{old('searchValue')}}">
                        <div class="input-group-btn">
                            <button class="btn btn-default" type="submit">
                                Tìm Kiếm
                                <i class="glyphicon glyphicon-search"></i>
                            </button>
                            <a href="{{route('Product.create')}}" class=" btn btn-primary" style="margin-left:3px">
                                <i class="glyphicon glyphicon-plus"></i>
                                Bổ sung
                            </a>
                        </div>
                    </div>
                </form>
                <div>
                    <p>Có <strong> {{$Product->total()}} </strong> kết quả trong tổng số
                        <strong>{{ $Product->lastPage() }}</strong> trang.
                    </p>
                    <table class="table table-hover table-bordered ">
                        <thead class="bg-blue-gradient ">
                                <th style="width:80px">Ảnh</th>
                                <th>Tên mặt hàng</th>
                                <th>Đơn vị tính</th>
                                <th>Giá</th>
                                <th style="width:100px">&nbsp;</th>
                        </thead>
                        <tbody>

                        @foreach ($Product as $items)
                            <tr>
                                <td>
                                    <img src="{{asset(str_replace("public/", "", $items->Photo))}}" style="width:80px" />
                                </td>
                                <td style="vertical-align:middle">{{$items->ProductName}}</td>
                                <td style="vertical-align:middle">{{$items->Unit}}</td>
                                <td style="vertical-align:middle">{{$items->Price}}</td>
                                <td class="text-right" style="vertical-align:middle">
                                    <a href="{{ route('Product.edit',$items->id)}}" class="btn btn-success btn-sm sm"><i  class="glyphicon glyphicon-edit"></i></a>
                                    <a href="{{ route('Product.show',$items->id)}}" onclick="return(confirm('Are you sure you want to delete this item?'))" class="btn btn-danger btn-sm sm"><i
                                            class="glyphicon glyphicon-trash"></i></a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                    <div class="text-center">
                        <nav aria-label="Page navigation example">

                        </nav>
                    </div>
                </div>


            </div>
        </div>

    </section>
    <!-- /.content -->

    <!-- phân trang -->
    <div class="text-center">
        <nav aria-label="Page navigation example">
            <ul class="pagination">
                <li class="page-item">
                    @if ($Product->currentPage() > 1)
                        <a href="{{ $Product->previousPageUrl() }}" class="page-link">&laquo; Previous</a>
                    @endif

                    @for ($i = 1; $i <= $Product->lastPage(); $i++)
                        <a href="{{ $Product->url($i) }}" class="page-link">{{ $i }}</a>
                    @endfor

                    @if ($Product->currentPage() < $Product->lastPage())
                        <a href="{{ $Product->nextPageUrl() }}" class="page-link">Next &raquo;</a>
                    @endif
                </li>
            </ul>
        </nav>
    </div>
@endsection


