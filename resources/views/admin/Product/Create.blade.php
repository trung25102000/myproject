﻿@extends('admin.layouts.app')
@section('content')
<div class="box box-primary">
    <div class="box-header with-border ">
        @if($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
    </div>

    <div class="box-body">
        <form class="form-horizontal" action="{{route('Product.store')}}" method="post"  enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="id">
            <div class="form-group">
                <label class="control-label col-sm-2">Tên mặt hàng:</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="ProductName" id="ProductName" autofocus>
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-sm-2">Loại hàng:</label>
                <div class="col-sm-10">
                    <select class="form-control" id="Category" name="categories_id">
                        <option>--Chọn Loại Hàng--</option>
                        @foreach($Category as $items)
                            <option value="{{$items->id}}" >{{$items->CategoryName}}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-sm-2">Nhà cung cấp:</label>
                <div class="col-sm-10">
                    <select class="form-control" id="Supplier" name="suppliers_id">
                        <option>--Chọn Nhà Cung Cấp--</option>
                        @foreach($Supplier as $items)
                            <option value="{{$items->id}}">{{$items->SupplierName}}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-sm-2">Đơn vị tính:</label>
                <div class="col-sm-10">
                    <input class="form-control" name="Unit" id="Unit" value="">
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-sm-2">Giá hàng:</label>
                <div class="col-sm-10">
                    <input class="form-control" name="Price" id="Price" value="">
                </div>
            </div>


            <div class="form-group">
                <label class="control-label col-sm-2">Ảnh minh họa:</label>
                <div class="col-sm-10">
                    <input type="file" class="form-control" name="UploadPhoto" onchange="document.getElementById('Photo').src = window.URL.createObjectURL(this.files[0])" />
                </div>
            </div>

            <div class="form-group">
                <div class="col-lg-offset-2 col-sm-10">
                    <img id="Photo" src="#" class="img img-bordered" style="width:200px" />
                </div>
            </div>
            <input type="hidden" name="Photo" value="">


            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-primary">
                        <i class="fa fa-save"></i> Lưu dữ liệu
                    </button>
                    <a class="btn btn-warning" href="{{route('Product.index')}}">
                        <i class="fa fa-ban"></i> Quay lại
                    </a>
                </div>
            </div>
        </form>
    </div>
    <div class="box-footer text-center">
    </div>

</div>
@endsection


