﻿<aside class="main-sidebar">
    <section class="sidebar">
        <ul class="sidebar-menu" data-widget="tree">
            <li class="treeview">
                <a href="/">
                    <i class="fa fa-dashboard"></i> <span>Trang chủ</span>
                </a>

            </li>
            <li class="treeview active">
                <a href="#">
                    <i class="fa fa-folder"></i> <span>Quản lý dữ liệu</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{route('Category.index')}}"><i class="fa fa-circle-o"></i> Loại hàng</a></li>
                    <li><a href="{{route('Product.index')}}"><i class="fa fa-circle-o"></i> Mặt hàng</a></li>
                    <li><a href="{{route('Supplier.index')}}"><i class="fa fa-circle-o"></i> Nhà cung cấp</a></li>
                    <li><a href="{{route('Customer.index')}}"><i class="fa fa-circle-o"></i> Khách hàng</a></li>
                    <li><a href="{{route('Shipper.index')}}"><i class="fa fa-circle-o"></i> Giao hàng</a></li>
                    <li><a href="{{route('Employee.index')}}"><i class="fa fa-circle-o"></i> Nhân viên</a></li>
                </ul>
            </li>
            <li class="treeview active">
                <a href="#">
                    <i class="fa fa-folder"></i> <span>Bán hàng</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{route('Order.create')}}"><i class="fa fa-circle-o"></i> Lập đơn hàng</a></li>
                    <li><a href="{{route('Order.index')}}"><i class="fa fa-circle-o"></i> Tra cứu đơn hàng</a></li>
                </ul>
            </li>

        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
