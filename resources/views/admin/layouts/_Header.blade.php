<header class="main-header">
    <!-- Biểu tượng -->
    <a href="/admin/dashboard" class="logo">
        <!-- biểu trưng nhỏ cho thanh bên nhỏ 50x50 pixel -->
        <span class="logo-mini">Mini</span>
        <!-- biểu trưng cho trạng thái thông thường và thiết bị di động -->
        <span class="logo-lg"><b>Trung Shop</b></span>
    </a>
    <!-- Header Navbar: kiểu có thể được tìm thấy trong header.less -->
    <nav class="navbar navbar-static-top">
        <!-- Nút chuyển đổi thanh bên-->
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Chuyển đổi điều hướng</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </a>
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <!-- Tài khoản người dùng: kiểu có thể được tìm thấy trong dropdown.less -->
                <li class="dropdown user user-menu">
                    <a href="" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="{{asset('Images/canhan.png')}}" class="user-image" alt="">
                        <span class="hidden-xs"> {{ Auth::user()->name }}</span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- Hình ảnh người dùng -->
                        <li class="user-header">
                            <img src="{{asset('Images/canhan.png')}}" class="img-circle" alt="">
                            <p>
                                 {{ Auth::user()->name }}

                                <small>{{Auth::user()->email }}</small>
                            </p>
                        </li>
                        <!--Chân trang trình đơn-->
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="#" class="btn btn-default btn-flat">Thông Tin</a>
                            </div>
                            <div class="pull-right">
                                <a href="{{ route('logout') }}" class="btn btn-default btn-flat"  onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    {{ __('Thoát') }}
                                </a>
                            </div>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                @csrf
                            </form>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>
