@extends('admin.layouts.app')
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            ORDER
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{route('admin.dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">ORDER</li>
        </ol>
        @if(session('success'))
            <div class="alert alert-success">
                {{ session('success') }}
            </div>
        @endif
        @if(session('message'))
            <div class="alert alert-success">
                {{ session('message') }}
            </div>
        @endif
    </section>
    <div class=" box box-primary">
        <div class="box-body">
            <!--Form đầu vào tìm kiếm-->
            <form id="formSearch">
                @csrf
                <div class="row">
                    <div class="col-sm-2">
                        <select class="form-control list" name="Status">
                            <option value="0">-- Trạng thái --</option>
                            <option value="Đơn hàng mới (chờ duyệt)!">Đơn hàng mới (chờ duyệt)</option>
                            <option value="Đơn hàng đã duyệt (chờ chuyển hàng)!">Đơn hàng đã duyệt (chờ chuyển hàng)</option>
                            <option value="Đơn hàng đang được giao">Đơn hàng đang được giao</option>
                            <option value="Đơn hàng đã hoàn tất thành công">Đơn hàng đã hoàn tất thành công</option>
                            <option value="Đơn hàng bị hủy">Đơn hàng bị hủy</option>
                            <option value="Đơn hàng bị từ chối">Đơn hàng bị từ chối</option>
                        </select>
                    </div>
                    <div class="col-sm-10 input-group">
                        <input type="text" name="searchValue"
                               class="form-control"
                               placeholder="Tìm kiếm theo tên khách hàng hoặc tên người giao hàng"
                                value="{{ session('searchValue') }}">
                        <span class="input-group-btn">
                        <button type="submit" name="search" id="search-btn" class="btn btn-flat btn-info">
                            <i class="fa fa-search"></i> Tìm kiếm
                        </button>
                    </span>
                    </div>
                </div>
            </form>
            <div id="OrderList">
                @include('admin.Order.Search',['Order'=>$Order])
            </div>
        </div>
        <script type="text/javascript">
            $(document).ready(function(){
                // Sử dụng AJAX để tìm kiếm và phân trang
                $("#formSearch").keyup(function (e) {
                    console.log('Hello World!');
                    e.preventDefault();
                    doSearch(1);
                    return;
                });
                $(".list").change(function () {
                    doSearch(1);
                    return;
                });
                $("#formSearch").submit(function (e) {
                    console.log('Hello World!');
                    e.preventDefault();
                    doSearch(1);
                    return;
                });
                doSearch({{$Order->currentPage()}});
                // Hàm thực hiện tìm kiếm và phân trang bằng AJAX
                function doSearch(page) {
                    var url = "{{route('Order.search')}}";
                    var postData = $("#formSearch").serializeArray();
                    postData.push({ "name": "page", "value": page });
                    $.ajax({
                        url: url,
                        type: "POST",
                        data: postData,
                        dataType: "html",
                        error: function () {
                            alert("Your request is not valid!");
                        },
                        success: function (data) {
                            $("#OrderList").html(data);
                            $(".page-link").click(function(e) {
                                e.preventDefault();
                                var page = $(this).attr('href').split('page=')[1];
                                doSearch(page);
                            });
                        }
                    });
                }
            });
        </script>
    </div>
@endsection



