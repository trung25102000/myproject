@extends('admin.layouts.app')
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            CREATE ORDER
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{route('admin.dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">CREATE ORDER</li>
        </ol>
        @if(session('success'))
            <div class="alert alert-success">
                {{ session('success') }}
            </div>
        @endif
        @if(session('error'))
            <div class="alert alert-danger">
                {{ session('error') }}
            </div>
        @endif
        @if(session('message'))
            <div class="alert alert-success">
                {{ session('message') }}
            </div>
        @endif
    </section>
    <div class=" box box-primary">
        <div class="box-body">
            <div class="col-md-4" style="border: 2px solid black;">
                <h4>Lựa chọn hàng cần bán</h4>
                <form id="formSearch" action="#">
                    @csrf
                    <div class="input-group">
                        <input type="text" name="searchValue"
                               class="form-control"
                               value="{{session('searchValue')}}"
                               placeholder="Nhập tên mặt hàng cần tìm">
                        <span class="input-group-btn">
                        <button type="submit" class="btn btn-flat btn-info">
                            <i class="fa fa-search"></i>
                        </button>
                    </span>
                    </div>
                </form>
                <div id="ProductList">
                    @include('admin.Order.SearchProduct',['Product'=>$Product])
                </div>
            </div>

            <div class="col-md-8">
                <!-- Giỏ hàng -->
                <h4>Danh sách mặt hàng đã chọn</h4>
                <table class="table table-bordered table-striped table-hover">
                    <thead>
                    <tr class="bg-primary">
                        <th class="text-center">STT</th>
                        <th class="text-center">Tên hàng</th>
                        <th class="text-center">ĐVT</th>
                        <th class="text-center">Số lượng</th>
                        <th class="text-center">Giá</th>
                        <th class="text-center">Thành tiền</th>
                        <th style="width:40px"></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $sumTotal = 0; ?>
                    @if(session()->has('cart'))
                    @foreach(session('cart') as $item)
                            <?php $sumTotal += $item['Price'] * $item['Quantity']; ?>
                        <tr>
                            <td class="text-center">{{$loop->index+1}}</td>
                            <td>{{$item['ProductName']}}</td>
                            <td class="text-center">{{$item['Unit']}}</td>
                            <td class="text-center">{{$item['Quantity']}}</td>
                            <td class="text-right">{{$item['Price']}}</td>
                            <td class="text-right">{{$item['Price'] * $item['Quantity']}}</td>
                            <td class="text-right">
                                <a href="{{route('Order.removeFromCart',$item['Product_id'])}}" class="btn btn-xs btn-danger" onclick="return confirm('Loại mặt hàng {{$item['ProductName']}} khỏi giỏ hàng?')">
                                    <i class="fa fa-minus"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    @else
                        <tr>
                            <td colspan="7" class="text-center">Giỏ hàng trống</td>
                        </tr>
                    @endif
                    </tbody>
                    <tfoot>
                    <tr>
                        <th colspan="5" class="text-right">Tổng cộng:</th>
                        <th class="text-right">{{$sumTotal}}</th>
                        <th></th>
                    </tr>
                    </tfoot>
                </table>
                <div class="text-right">
                    <a href="{{route('Order.RemoveCart',0)}}" class="btn btn-sm btn-danger" onclick="return confirm('Có muốn xóa giỏ hàng không?')">
                        <i class="fa fa-trash"></i> Xóa giỏ hàng
                    </a>
                </div>

                <!-- Nhập khách hàng, nhân viên phụ trách và khởi tạo đơn hàng -->
                <form action="{{Route('Order.Init')}}" class="form-horizontal" method="post">
                    @csrf
                    <h4>Nhập thông tin về khách hàng và nhân viên phụ trách đơn hàng</h4>
                    <div class="form-group">
                        <div class="col-md-6">
                            <label>Khách hàng:</label>
                            <select class="form-control" name="customerID">
                                <option value="0">-- Chọn khách hàng --</option>
                                @foreach($Customer as $item)
                                    <option value="{{$item->id}}">{{$item->CustomerName}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label>Nhân viên phụ trách:</label>
                            <select class="form-control" name="employeeID">
                                <option value="0">-- Chọn nhân viên --</option>
                                @foreach($Employee as $item)
                                    <option value="{{$item->id}}">{{$item->FullName}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12 text-right">
                            <button type="submit" class="btn btn-sm btn-primary">Lập đơn hàng</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <script type="text/javascript">
            $(document).ready(function(){
                // Sử dụng AJAX để tìm kiếm và phân trang
                $("#formSearch").keyup(function (e) {
                    console.log('Hello World!');
                    e.preventDefault();
                    doSearch(1);
                    return;
                });
                $(".list").change(function () {
                    doSearch(1);
                    return;
                });
                $("#formSearch").submit(function (e) {
                    console.log('Hello World!');
                    e.preventDefault();
                    doSearch(1);
                    return;
                });
                doSearch({{$Product->currentPage()}});
                // Hàm thực hiện tìm kiếm và phân trang bằng AJAX
                function doSearch(page) {
                    var url = "{{route('Order.searchproduct')}}";
                    var postData = $("#formSearch").serializeArray();
                    postData.push({ "name": "page", "value": page });

                    $.ajax({
                        url: url,
                        type: "POST",
                        data: postData,
                        dataType: "html",
                        error: function () {
                            alert("Your request is not valid!");
                        },
                        success: function (data) {
                            $("#ProductList").html(data);
                            $(".page-link").click(function(e) {
                                e.preventDefault();
                                var page = $(this).attr('href').split('page=')[1];
                                doSearch(page);
                            });
                        }
                    });
                }
            });
        </script>
    </div>
@endsection



