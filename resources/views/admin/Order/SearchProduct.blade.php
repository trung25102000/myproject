<div>
    @foreach($Product as $item)
        <form action="{{route('Order.AddToCart')}}" method="post" style="background-color:#fff; border:1px solid #ccc; margin:5px 0px 5px 0px; padding:5px;">
            @csrf
            <input type="hidden" value="{{$item->id}}" name="products_id" />
            <input type="hidden" name="ProductName" value="{{$item->ProductName}}" />
            <input type="hidden" name="Unit" value="{{$item->Unit}}" />
            <input type="hidden" name="Photo" value="{{$item->Photo}}" />

            <div class="row" >
                <div class="col-md-12">
                    <strong>{{$item->ProductName}}</strong>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3" >
                    <img class="img img-responsive img-bordered" src="{{asset(str_replace("public/", "", $item->Photo))}}">
                </div>
                <div class="col-sm-9">
                    <div class="form-group-sm">
                        <div class="row">
                            <div class="col-md-7">
                                <label>Giá bán:</label>
                                <input class="form-control" type="text" value="{{$item->Price}}" name="SalePrice" />
                            </div>
                            <div class="col-md-5">
                                <label>Số lượng:</label>
                                <input class="form-control" type="number" value="1" min="1" name="Quantity" />
                            </div>
                        </div>
                    </div>
                    <div class="form-group-sm" style="margin-top:2px;">
                        <button type="submit" class="btn btn-sm btn-primary">
                            <i class="fa fa-shopping-cart"></i> Thêm vào giỏ
                        </button>
                    </div>
                </div>
            </div>
        </form>
    @endforeach
</div>

<nav aria-label="Page navigation example">
    <ul class="pager">
        <li class="page-item">
            @if ($Product->currentPage() > 1)
                <a href="{{ $Product->previousPageUrl() }}" class="page-link">&laquo; Previous</a>
            @endif

            @for ($i = 1; $i <= $Product->lastPage(); $i++)
                <a href="{{ $Product->url($i) }}" class="page-link">{{ $i }}</a>
            @endfor

            @if ($Product->currentPage() < $Product->lastPage())
                <a href="{{ $Product->nextPageUrl() }}" class="page-link">Next &raquo;</a>
            @endif
        </li>
    </ul>
</nav>
