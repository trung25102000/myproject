@extends('admin.layouts.app')
@section('content')
    <div class="box box-primary">
        <div class="box-header text-right">
            <div class="btn-group">
                <!-- //TODO
                YÊU CẦU: Đối với các chức năng liên quan đến đơn hàng, chỉ được hiển thị menu/nút của những chức năng
                phù hợp với trạng thái hiện tại của đơn hàng (ví dụ: Nếu đơn hàng chưa được chấp nhận thì không được chuyển để giao hàng)
                -->
                <div class="btn-group">
                    <button type="button" class="btn btn-sm btn-primary dropdown-toggle" data-toggle="dropdown">
                        Xử lý đơn hàng <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="{{route('Order.Accept',$order->id)}}" onclick="return confirm('Xác nhận duyệt chấp nhận đơn hàng này?')">Duyệt đơn hàng</a></li>
                        <li><a href="{{route('Order.Shipping',$order->id)}}" class="btn-modal">Chuyển giao hàng</a></li>
                        <li><a href="{{route('Order.Finished',$order->id)}}" onclick="return confirm('Xác nhận đơn hàng đã hoàn tất thành công?')">Xác nhận hoàn tất đơn hàng</a></li>
                        <li class="divider"></li>
                        <li><a href="{{route('Order.Cancel',$order->id)}}" onclick="return confirm('Xác nhận hủy đơn hàng này?')">Hủy đơn hàng</a></li>
                        <li><a href="{{route('Order.Reject',$order->id)}}" onclick="return confirm('Xác nhận từ chối đơn hàng này?')">Từ chối đơn hàng</a></li>
                    </ul>
                </div>
                <div class="btn-group">
                <form action="{{ route('Order.destroy',$order->id) }}" method="POST">
                    @csrf
                    @method('delete')
                    <button type="submit" class="btn btn-sm btn-danger" onclick="return confirm('Có chắc chắn muốn xóa đơn hàng này không?')">
                        <i class="fa fa-trash"></i> Xóa đơn hàng
                    </button>
                </form>
                </div>
                <a href="{{URL::previous()}}" class="btn btn-sm btn-info">Quay lại</a>
            </div>
        </div>
        <div class="box-body form-horizontal">
            <div class="form-group">
                <label class="control-label col-sm-2">Mã đơn hàng:</label>
                <div class="col-sm-4">
                    <p class="form-control-static">{{$order->id}}</p>
                </div>
                <label class="control-label col-sm-2">Ngày lập đơn hàng:</label>
                <div class="col-sm-4">
                    <p class="form-control-static">{{$order->OrderTime}}</p>
                </div>
                <label class="control-label col-sm-2">Nhân viên phụ trách:</label>
                <div class="col-sm-4">
                    <p class="form-control-static">{{$Employee->FullName}}</p>
                </div>
                <label class="control-label col-sm-2">Ngày nhận đơn hàng:</label>
                <div class="col-sm-4">
                    <p class="form-control-static">{{$order->AcceptTime}}</p>
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-sm-2">Khách hàng:</label>
                <div class="col-sm-4">
                    <p class="form-control-static">{{$Customer->CustomerName}}</p>
                </div>
                <label class="control-label col-sm-2">Tên giao dịch:</label>
                <div class="col-sm-4">
                    <p class="form-control-static">{{$Customer->ContactName}}</p>
                </div>
                <label class="control-label col-sm-2">Địa chỉ:</label>
                <div class="col-sm-10">
                    <p class="form-control-static">{{$Customer->Address}}</p>
                </div>
                <label class="control-label col-sm-2">Email:</label>
                <div class="col-sm-4">
                    <p class="form-control-static">{{$Customer->Email}}</p>
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-sm-2">Người giao hàng:</label>
                <div class="col-sm-4">
                    @if($order->shipper_id!=null)
                        <p class="form-control-static">{{\App\Models\Shipper::find($order->shipper_id)->ShipperName}}</p>
                    @endif
                </div>
                <label class="control-label col-sm-2">Điện thoại:</label>
                <div class="col-sm-4">
                    @if($order->shipper_id!=null)
                        <p class="form-control-static">{{\App\Models\Shipper::find($order->shipper_id)->Phone}}</p>
                    @endif
                </div>
                <label class="control-label col-sm-2">Nhận giao hàng lúc:</label>
                <div class="col-sm-4">
                    <p class="form-control-static">{{$order->ShippedTime}}</p>
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-sm-2">Trạng thái đơn hàng:</label>
                <div class="col-sm-4">
                    <p class="form-control-static">{{$order->Status}}</p>
                </div>
                <label class="control-label col-sm-2">Thời điểm hoàn tất:</label>
                <div class="col-sm-4">
                    <p class="form-control-static">{{$order->FinishedTime}}</p>
                </div>
            </div>
        </div>

        <div class="box-body">
            <div class="table-responsive">
                <table class="table table-bordered table-striped table-hover">
                    <caption><h4>Danh sách mặt hàng thuộc đơn hàng</h4></caption>
                    <thead>
                    <tr class="bg-primary">
                        <th class="text-center">STT</th>
                        <th class="text-center">Tên hàng</th>
                        <th class="text-center">ĐVT</th>
                        <th class="text-center">Số lượng</th>
                        <th class="text-center">Giá</th>
                        <th class="text-center">Thành tiền</th>
                        <th style="width:80px"></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $sumTotal = 0; ?>
                    @if($OrderDetailsList->count()>0)
                        @foreach($OrderDetailsList as $item)
                                <?php $sumTotal += $item->SalePrice * $item->Quantity; $Product=\App\Models\Products::find($item->products_id); ?>
                            <tr>
                                <td class="text-center">{{$loop->index+1}}</td>
                                <td>{{$Product->ProductName}}</td>
                                <td class="text-center">{{$Product->Unit}}</td>
                                <td class="text-center">{{$item->Quantity}}</td>
                                <td class="text-right">{{$item->SalePrice}}</td>
                                <td class="text-right">{{$item->SalePrice * $item->Quantity}}</td>
                                <td class="text-right">
                                    <a href="{{route('Order.RemoveDetail',[$item->products_id,$item->order_id])}}" class="btn btn-xs btn-danger" onclick="return confirm('Loại mặt hàng {{$item['ProductName']}} khỏi giỏ hàng?')">
                                        <i class="fa fa-minus"></i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="7" class="text-center">Giỏ hàng trống</td>
                        </tr>
                    @endif
                    </tbody>
                    <tfoot>
                    <tr>
                        <th colspan="5" class="text-right">Tổng cộng:</th>
                        <th class="text-right">{{$sumTotal}}</th>
                        <th></th>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>

    <div id="dialogModal" class="modal fade" role="dialog">
        @include('admin.Order.Shipping',['Shipper'=>$ShipperList,'Order'=>$order])
    </div>

<script>
    $(document).ready(function () {
        $(".btn-modal").click(function (e) {
            e.preventDefault();
            var link = $(this).prop("href");
            console.log(link);
            $.ajax({
                url: link,
                type: "GET",
                async: false,
                error: function () {
                    alert("Your request is not valid!");
                },
                success: function (data) {
                    $("#dialogModal").empty();
                    $("#dialogModal").html(data);
                    $("#dialogModal").modal();
                }
            });
        });
    })
</script>
@endsection

