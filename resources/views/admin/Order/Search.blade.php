@if($Order->total()>0)
    <p>Có <strong>{{$Order->total()}}</strong> đơn hàng trong tổng số <strong>{{$Order->lastPage()}}</strong> trang. </p>

    <table class="table table-hover table-bordered ">
        <thead class="bg-blue-gradient ">
        <tr class="bg-primary">
            <th>Khách hàng</th>
            <th>Ngày lập</th>

            <th>Nhân viên phụ trách</th>
            <th>Thời điểm duyệt</th>
            <th>Người giao hàng</th>
            <th>ngày nhận giao hàng</th>
            <th>Thời điểm kết thúc</th>
            <th>Trạng thái</th>
            <th style="width:40px"></th>
        </tr>
        </thead>
        <tbody>
        @foreach($Order as $item)
            <tr>
                <td>{{$item->CustomerName}}</td>
                <td>{{$item->OrderTime}}</td>
                <td>{{$item->EmployeeFullName}}</td>
                <td>{{$item->AcceptTime}}</td>
                <td>{{$item->ShipperName}}</td>
                <td>{{$item->ShippedTime}}</td>
                <td>{{$item->FinishedTime}}</td>
                <td>{{$item->Status}}</td>
                <td>
                        <a href="{{route('Order.Details',$item->id)}}}" class="btn btn-info btn-xs">
                            <i class="glyphicon glyphicon-th-list"></i>
                        </a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <!-- phân trang -->
    <div class="text-center">
        <nav aria-label="Page navigation example">
            <ul class="pagination">
                <li class="page-item">
                    @if ($Order->currentPage() > 1)
                        <a href="{{ $Order->previousPageUrl() }}" class="page-link">&laquo; Previous</a>
                    @endif

                    @for ($i = 1; $i <= $Order->lastPage(); $i++)
                        <a href="{{ $Order->url($i) }}" class="page-link">{{ $i }}</a>
                    @endfor

                    @if ($Order->currentPage() < $Order->lastPage())
                        <a href="{{ $Order->nextPageUrl() }}" class="page-link">Next &raquo;</a>
                    @endif
                </li>
            </ul>
        </nav>
    </div>
@else
    <h2>Không tìm thấy dữ liệu muốn tìm</h2>
@endif
