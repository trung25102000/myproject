<div class="modal-dialog">
    <div class="modal-content">
        <form action="{{route('Order.Shipped')}}" method="post">
            @csrf
            <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Cập nhật chi tiết đơn hàng</h4>
            </div>
          <input type="hidden" name="OrderId" value="{{$Order->id}}">
            <div class="modal-body">
                <div class="form-group">
                    <label class="control-label">Người giao hàng:</label>
                    <select name="ShipperID" class="form-control">
                        <option value="0">-- Chọn người giao hàng ---</option>
                        @foreach($Shipper as $item)
                            <option value="{{$item->id}}">{{$item->ShipperName}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">
                    <i class="fa fa-floppy-o"></i> Cập nhật
                </button>
                <button type="button" class="btn btn-default" data-dismiss="modal">
                    Bỏ qua
                </button>
            </div>
        </form>
    </div>
</div>
