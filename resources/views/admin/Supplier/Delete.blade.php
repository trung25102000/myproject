﻿@extends('admin.layouts.app');
@section('content')
    <div class="box box-danger">
        <div class="box-body">
            @if($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form class="form-horizontal" action="{{ route('Supplier.destroy',$Supplier->id) }}" method="post">
                @csrf
                @method('Delete')
                <div class="form-group">
                    <div class="col-sm-10">
                        <input type="hidden" class="form-control" id="SupplierID" name="SupplierID" value="{{$Supplier->id}}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2">Tên nhà cung cấp:</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="SupplierName" name="SupplierName" value="{{$Supplier->SupplierName}}" autofocus>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-2" >Tên giao dịch:</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="ContactName" name="ContactName" value="{{$Supplier->ContactName}}">
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-2">Địa chỉ:</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="Address" name="Address" value="{{$Supplier->Address}}">
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-2">Số điện thoại:</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="Phone" name="Phone" value="{{$Supplier->Phone}}">
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-2">Quốc gia:</label>
                    <div class="col-sm-10">
                        <select class="form-control" id=" Country" name="Country">
                            <option>--Chọn Quốc Gia--</option>
                            <option value="{{$Supplier->Country}}" selected>{{$Supplier->Country}}</option>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-2">Thành phố:</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="City" name="City" value="{{$Supplier->City}}">
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-2">Mã bưu chính:</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="PostalCode" name="PostalCode" value="{{$Supplier->PostalCode}}">
                    </div>
                </div>

                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-danger ">
                        <i class="glyphicon glyphicon-trash"></i>
                        Xóa dữ liệu
                    </button>
                    <a href="{{route('Supplier.index')}}" class="btn btn-warning">quay lại</a>
                </div>

            </form>
        </div>
    </div>
@endsection
