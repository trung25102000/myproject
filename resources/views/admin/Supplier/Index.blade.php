﻿@extends('admin.layouts.app')
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            SUPPLIER
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{route('admin.dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">SUPPLIER</li>
        </ol>
        @if(session('success'))
            <div class="alert alert-success">
                {{ session('success') }}
            </div>
        @endif
        @if(session('message'))
            <div class="alert alert-success">
                {{ session('message') }}
            </div>
        @endif
    </section>

    <!-- Main content -->
    <section class="content">
        <div class=" box box-primary">
            <div class="box-body">
                <form id="formSearch" action="{{ route('Supplier.search') }}" method="get">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Search" name="searchValue" autofocus
                               value="{{old('searchValue')}}">
                        <div class="input-group-btn">
                            <button class="btn btn-default" type="submit">
                                Tìm Kiếm
                                <i class="glyphicon glyphicon-search"></i>
                            </button>
                            <a href="{{route('Supplier.create')}}" class=" btn btn-primary" style="margin-left:3px">
                                <i class="glyphicon glyphicon-plus"></i>
                                Bổ sung
                            </a>
                        </div>
                    </div>
                </form>
                <div>
                    <p>Có <strong> {{$Supplier->total()}} </strong> kết quả trong tổng số
                        <strong>{{ $Supplier->lastPage() }}</strong> trang.
                    </p>
                    <table class="table table-hover table-bordered ">
                        <thead class="bg-blue-gradient ">
                            <th>STT</th>
                            <th>Tên nhà cung cấp</th>
                            <th>Tên giao dịch</th>
                            <th>Địa chỉ</th>
                            <th>Số điện thoại</th>
                            <th>Quốc gia</th>
                            <th>Thành phố </th>
                            <th>Mã bưu chính</th>
                            <th style="width:100px"></th>
                        </thead>
                        <tbody>

                        @foreach ($Supplier as $items)
                            <tr>
                                <td>{{ $loop->index+1 }}</td>
                                <td>{{$items->SupplierName}}</td>
                                <td>{{$items->ContactName}}</td>
                                <td>{{$items->Address}}</td>
                                <td>{{$items->Phone}}</td>
                                <td>{{$items->Country}}</td>
                                <td>{{$items->City}}</td>
                                <td>{{$items->PostalCode}}</td>
                                <td>
                                    <a href="{{ route('Supplier.edit',$items->id)}}" class="btn btn-success btn-sm sm"><i  class="glyphicon glyphicon-edit"></i></a>
                                    <a href="{{ route('Supplier.show',$items->id)}}" class="btn btn-danger btn-sm sm"><i
                                            class="glyphicon glyphicon-trash"></i></a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                    <div class="text-center">
                        <nav aria-label="Page navigation example">

                        </nav>
                    </div>
                </div>


            </div>
        </div>

    </section>
    <!-- /.content -->

    <!-- phân trang -->
    <div class="text-center">
        <nav aria-label="Page navigation example">
            <ul class="pagination">
                <li class="page-item">
                    @if ($Supplier->currentPage() > 1)
                        <a href="{{ $Supplier->previousPageUrl() }}" class="page-link">&laquo; Previous</a>
                    @endif

                    @for ($i = 1; $i <= $Supplier->lastPage(); $i++)
                        <a href="{{ $Supplier->url($i) }}" class="page-link">{{ $i }}</a>
                    @endfor

                    @if ($Supplier->currentPage() < $Supplier->lastPage())
                        <a href="{{ $Supplier->nextPageUrl() }}" class="page-link">Next &raquo;</a>
                    @endif
                </li>
            </ul>
        </nav>
    </div>

@endsection



