@extends('admin.layouts.app')
@section('content')
    <div class="box box-primary">
        <div class="box-body">
            @if($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form class="form-horizontal" action="{{ route('Supplier.store') }}" method="post">
                @csrf
                <div class="form-group">
                    <div class="col-sm-10">
                        <input type="hidden" class="form-control" id="SupplierID" name="SupplierID" value="">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2">Tên nhà cung cấp:</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="SupplierName" name="SupplierName" value="" autofocus>
                    </div>

                </div>

                <div class="form-group">
                    <label class="control-label col-sm-2" >Tên giao dịch:</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="ContactName" name="ContactName" value="">
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-2">Địa chỉ:</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="Address" name="Address" value="">
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-2">Số điện thoại:</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="Phone" name="Phone" value="">
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-2">Quốc gia:</label>
                    <div class="col-sm-10">
                        <select class="form-control" id=" Country" name="Country">
                            <option>--Chọn Quốc Gia--</option>
                            <option value="Viet nam" selected>Viet nam</option>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-2">Thành phố:</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="City" name="City" value="">
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-2">Mã bưu chính:</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="PostalCode" name="PostalCode" value="">
                    </div>
                </div>

                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-default bg-blue">
                        <i class="glyphicon glyphicon-floppy-disk"></i>
                        Lưu dữ liệu
                    </button>
                    <a href="{{route('Supplier.index')}}" class="btn btn-warning">quay lại</a>
                </div>

            </form>
        </div>
    </div>
@endsection
