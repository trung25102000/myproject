﻿@if($Shipper->total()>0)
    <p>Có <strong>{{$Shipper->total()}}</strong> nhà cung cấp trong tổng số <strong>{{$Shipper->lastPage()}}</strong> trang. </p>

    <table class="table table-hover table-bordered ">
        <thead class="bg-blue-gradient ">
        <th>STT</th>
        <th>Tên Nguười giao hàng</th>
        <th>Số điện thoại</th>
        <th></th>
        </thead>
        <tbody>
        @foreach($Shipper as $item)
            <tr>
                <td>{{ $loop->index+1 }}</td>
                <td>{{$item->ShipperName}}</td>
                <td>{{$item->Phone}}</td>
                <td >
                    <a href="{{route('Shipper.edit',$item->id)}}" class="btn btn-success btn-sm "><i class="glyphicon glyphicon-edit"></i> </a>
                    <a href="{{route('Shipper.show',$item->id)}}" class="btn btn-danger btn-sm "><i class="glyphicon glyphicon-trash"></i></a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <!-- phân trang -->
    <div class="text-center">
        <nav aria-label="Page navigation example">
            <ul class="pagination">
                <li class="page-item">
                    @if ($Shipper->currentPage() > 1)
                        <a href="{{ $Shipper->previousPageUrl() }}" class="page-link">&laquo; Previous</a>
                    @endif

                    @for ($i = 1; $i <= $Shipper->lastPage(); $i++)
                        <a href="{{ $Shipper->url($i) }}" class="page-link">{{ $i }}</a>
                    @endfor

                    @if ($Shipper->currentPage() < $Shipper->lastPage())
                        <a href="{{ $Shipper->nextPageUrl() }}" class="page-link">Next &raquo;</a>
                    @endif
                </li>
            </ul>
        </nav>
    </div>
@else
    <h2>Không tìm thấy dữ liệu muốn tìm</h2>
@endif
