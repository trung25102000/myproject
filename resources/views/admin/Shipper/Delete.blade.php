@extends('admin.layouts.app')
@section('content')
    <div class="box box-danger">
        <div class="box-body">
            @if($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form class="form-horizontal" action="{{route("Shipper.destroy",$Shipper->id)}}" method="post">
                @csrf
                @method('delete')
                <div class="form-group">
                    <label class="control-label col-sm-2">Tên người giao hàng:</label>
                    <div class="col-sm-10">
                        <input  type="text" class="form-control" id="ShipperName" name="ShipperName" value="{{$Shipper->ShipperName}}" readonly>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-2">Số điện thoại:</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="Phone" name="Phone" value="{{$Shipper->Phone}}" readonly>
                    </div>
                </div>



                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-danger ">
                            <i class="glyphicon glyphicon-trash"></i>
                            Xóa dữ liệu
                        </button>
                        <a href="{{route('Shipper.index')}}" class="btn btn-warning">quay lại</a>
                    </div>
                </div>

            </form>
        </div>
    </div>
@endsection

