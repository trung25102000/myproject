@extends('admin.layouts.app')
@section('content')
    <div class="box box-primary">
        <div class="box-body">
            @if($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form class="form-horizontal" action="{{route("Shipper.store")}}" method="post">
                @csrf
                <div class="form-group">
                    <label class="control-label col-sm-2">Tên người giao hàng:</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="ShipperName" name="ShipperName" value="{{old('ShipperName')}}" autofocus>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-2">Số điện thoại:</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="Phone" name="Phone" value="{{old('Phone')}}">
                    </div>
                </div>



                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-default bg-blue">
                            <i class="glyphicon glyphicon-floppy-disk"></i>
                            Lưu dữ liệu
                        </button>
                        <a href="{{route('Shipper.index')}}" class="btn btn-warning">quay lại</a>
                    </div>
                </div>

            </form>
        </div>
    </div>

@endsection

