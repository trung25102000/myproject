<div class="container-fluid pt-5">
    <div class="row px-xl-5 pb-3">
        <div class="col-lg-3 col-md-6 col-sm-12 pb-1">
            <div class="d-flex align-items-center border mb-4" style="padding: 30px;">
                <h1 class="fa fa-check text-primary m-0 mr-3"></h1>
                <h5 class="font-weight-semi-bold m-0">Quality Product</h5>
            </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-12 pb-1">
            <div class="d-flex align-items-center border mb-4" style="padding: 30px;">
                <h1 class="fa fa-shipping-fast text-primary m-0 mr-2"></h1>
                <h5 class="font-weight-semi-bold m-0">Free Shipping</h5>
            </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-12 pb-1">
            <div class="d-flex align-items-center border mb-4" style="padding: 30px;">
                <h1 class="fas fa-exchange-alt text-primary m-0 mr-3"></h1>
                <h5 class="font-weight-semi-bold m-0">14-Day Return</h5>
            </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-12 pb-1">
            <div class="d-flex align-items-center border mb-4" style="padding: 30px;">
                <h1 class="fa fa-phone-volume text-primary m-0 mr-3"></h1>
                <h5 class="font-weight-semi-bold m-0">24/7 Support</h5>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid pt-5">
    <div class="text-center mb-4">
        <h2 class="section-title px-5"><span class="px-2">Products</span></h2>
    </div>
    <div><@if($products->count()>0)
    <div class="row px-xl-5 pb-3">
        @foreach ($products as $item)
            <div class="col-lg-3 col-md-6 col-sm-12 pb-1">
                <div class="card product-item border-0 mb-4">
                    <div class="card-header product-img position-relative overflow-hidden bg-transparent border p-0">
                        <img class="img-fluid w-100"
                             src="{{asset(str_replace("public/", "", $item->Photo))}}"
                             alt="">
                    </div>
                    <div class="card-body border-left border-right text-center p-0 pt-4 pb-3">
                        <h6 class="text-truncate mb-3">{{ $item->ProductName }}</h6>
                        <div class="d-flex justify-content-center">
                            <h6>${{ $item->Price }}</h6>
                            @if($item->SalePrice>0)
                            <h6 class="text-muted ml-2"><del>${{$item->Price+$item->SalePrice}}</del></h6>
                            @endif
                        </div>
                    </div>
                    <div class="card-footer d-flex justify-content-between bg-light border">
                        <a href="/Client/Product/Details/{{$item->id}}" id="ProductDetails" class="btn btn-sm text-dark p-0"><i
                                class="fas fa-eye text-primary mr-1"></i>View
                            Detail</a>
                        <a href="/Client/Product/AddToCart/{{$item->id}}" class="btn btn-sm text-dark p-0"><i
                                class="fas fa-shopping-cart text-primary mr-1"></i>Add To Cart</a>
                    </div>
                </div>
            </div>
        @endforeach ()
    </div>
<!-- phân trang -->
<div class="text-center">
    <nav aria-label="Page navigation example">
        <ul class="pagination d-inline-flex">
            <li class="page-item">
                @if ($products->currentPage() > 1)
                    <a href="{{ $products->previousPageUrl() }}" class="page-link">&laquo; Previous</a>
                @endif
            </li>

            @for ($i = 1; $i <= $products->lastPage(); $i++)
                <li class="page-item">
                    <a href="{{ $products->url($i) }}" class="page-link">{{ $i }}</a>
                </li>
            @endfor

            <li class="page-item">
                @if ($products->currentPage() < $products->lastPage())
                    <a href="{{ $products->nextPageUrl() }}" class="page-link">Next &raquo;</a>
                @endif
            </li>
        </ul>
    </nav>
</div>
@else
<h1>Không có mặt hàng phù hợp.</h1>
@endif
    </div>
</div>
