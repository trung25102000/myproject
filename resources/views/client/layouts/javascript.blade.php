<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.bundle.min.js"></script>
<script src="{{ asset('client/lib/easing/easing.min.js') }}"></script>
<script src="{{ asset('client/lib/owlcarousel/owl.carousel.min.js') }}"></script>

<!-- Contact Javascript File -->
<script src="mail/jqBootstrapValidation.min.js"></script>
<script src="{{ asset('client/mail/contact.js') }}"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.21/lodash.min.js"
integrity="sha512-WFN04846sdKMIP5LKNphMaWzU7YpMyCU245etK3g/2ARYbPK9Ub18eG+ljU96qKRCWh+quCY7yefSmlkQw1ANQ=="
crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<!-- Template Javascript -->
<script src="{{ asset('client/js/main.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function(){
        // Sử dụng AJAX để tìm kiếm và phân trang
        $("#formSearch").keyup(function (e) {
            console.log('Hello World!');
            e.preventDefault();

            doSearch(1);
            return;
        });
        $("#ProductDetails").click(function (e) {
            console.log('Hello World!');
            e.preventDefault();

            doResuft();
            return;
        });
        $("#formSearch").submit(function (e) {
            console.log('Hello World!');
            e.preventDefault();

            doSearch(1);
            return;
        });
        doSearch({{$products->currentPage()}});

        function doResuft()
        {
            var url=$(this).attr('href');
            $.ajax({
                url:url,
                type:"POST",
                dataType: "html",
                error: function () {
                    alert("Your request is not valid!");
                },
                success: function (data) {
                    $("#ProductList").html(data);}
            })

        }
        // Hàm thực hiện tìm kiếm và phân trang bằng AJAX
        function doSearch(page,categoryID) {
            var url = "/Client/Product/Search";
            var postData = $("#formSearch").serializeArray();
            postData.push({ "name": "page", "value": page });
            postData.push({ "name": "CategoryID", "value": categoryID });
            $.ajax({
                url: url,
                type: "POST",
                data: postData,
                dataType: "html",
                error: function () {
                    alert("Your request is not valid!");
                },
                success: function (data) {
                    $("#ProductList").html(data);
                    $(".page-link").click(function(e) {
                        e.preventDefault();
                        var page = $(this).attr('href').split('page=')[1];

                        doSearch(page);
                    });
                    $(".nav-link").click(function(e) {
                        e.preventDefault();
                        var categoryID = $(this).attr('href').split('CategoryID=')[1];

                        doSearch(1,categoryID);
                    });
                }
            });
        }
    });
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>
<script src="{{ asset('admin/assets/base/base.js') }}"></script>

@yield('script')
