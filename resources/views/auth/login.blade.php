<!DOCTYPE html>
<html lang="en">
<head>
    <title>Login V1</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--===============================================================================================-->
    <link rel="icon" type="image/png" href="{{asset('admin/Images/icons/favicon.ico')}}"/>
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{asset('admin/vendor/bootstrap/css/bootstrap.min.css')}}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{asset('admin/fonts/font-awesome-4.7.0/css/font-awesome.min.css')}}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{asset('admin/vendor/animate/animate.css')}}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{asset('admin/vendor/css-hamburgers/hamburgers.min.css')}}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{asset('admin/vendor/select2/select2.min.css')}}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{asset('admin/css/util.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('admin/css/main.css')}}">
    <!--===============================================================================================-->
    <style>
        .wrap-login100 {
            margin-top: 50px;
            margin-left: 80px;
            margin-bottom: 120px;
            margin-right: 90px;
            height: 600px;
        }

    </style>
</head>
<body>

<div class="limiter">
    <div class="container-login100">
        <div style="font-weight:bold; color:#fff;display:inline-block; font-size: 24px;">
            <h1>Login Page</h1>
        </div>
        <div class="wrap-login100">
            <div class="login100-pic js-tilt" data-tilt style=";padding-bottom: 50px;margin-top: -70px;">
                <img src="Images/Group 3653.png" alt="IMG">
                <p style="padding-top: 70px;padding-left: 70px;">Copyright Reserved@2021</p>
            </div>
            <form class="login100-form validate-form" style="padding-left: 30px;padding-bottom: 50px;margin-top: -70px;margin-right: -20px" action="{{ route('login') }}" method="post">
					<span class="login100-form-title">
                        <h1>Welcome Back!</h1>
                        <p>Login to continue</p>
					</span>
                @csrf
                <div class="wrap-input100 validate-input" data-validate = "Valid email is required: ex@abc.xyz">
                    <input class="input100 form-control @error('email') is-invalid @enderror" name="email"
                           value="{{ old('email') }}" required autocomplete="email" autofocus>
                    <span class="focus-input100"></span>
                    @error('email')
                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                    @enderror
                    <span class="symbol-input100">
							<i class="fa fa-user" aria-hidden="true"></i>
						</span>
                </div>

                <div class="wrap-input100 validate-input" data-validate = "Password is required">
                    <input id="password" type="password" class="input100 form-control @error('password') is-invalid @enderror" name="password"
                           required autocomplete="current-password">
                    <span class="focus-input100"></span>
                    @error('password')
                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                    @enderror
                    <span class="symbol-input100">
							<i class="fa fa-lock" aria-hidden="true"></i>
						</span>
                </div>
                <div class="remember-forgot">
                    <div><input type="checkbox" class="form-check-input" name="remember"
                                id="remember" {{ old('remember') ? 'checked' : '' }}> {{ __('Remember Me') }}</div>
                    @if (Route::has('password.request'))
                    <a class="btn btn-link" href="{{ route('password.request') }}">Forgot Password?</a>
                    @endif
                </div>
                <div class="container-login100-form-btn">
                    <button class="login100-form-btn">
                        {{ __('Sign in') }}
                    </button>
                </div>

                <div class="text-center">
                    @if (Route::has('register'))
                    <a class="txt2 nav-link" href="{{ route('register') }}">
                        {{__('New User? Sign up')}}
                    </a>
                    @endif
                </div><br>
                <a href="#"style="padding-top:25px; padding-right: 45px ;display:flex; justify-content: flex-end;text-decoration:underline;">Terms and Conditions | Privacy Policy</a>
            </form>
        </div>
    </div>
</div>




<!--===============================================================================================-->
<script src="{{asset('admin/vendor/jquery/jquery-3.2.1.min.js')}}"></script>
<!--===============================================================================================-->
<script src="{{asset('admin/vendor/bootstrap/js/popper.js')}}"></script>
<script src="{{asset('adminvendor/bootstrap/js/bootstrap.min.js')}}"></script>
<!--===============================================================================================-->
<script src="{{asset('admin/vendor/select2/select2.min.js')}}"></script>
<!--===============================================================================================-->
<!-- <script src="vendor/tilt/tilt.jquery.min.js"></script>
<script >
    $('.js-tilt').tilt({
        scale: 1.1
    })
</script> -->
<!--===============================================================================================-->
<script src="{{asset('admin/js/main.js')}}"></script>

</body>
</html>
